import unittest

from common_primitives import dataset_to_dataframe, dataset_remove_columns
from d3m.metadata import base as metadata_base

import utils as test_utils


class RemoveColumnsPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):
        # load the iris dataset
        dataset = self._loadAndRemove()

        expected = ['d3mIndex', 'sepalWidth', 'petalWidth', 'species']

        # validate metadata
        num_columns = dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS,))['dimension']['length']
        self.assertEqual(len(expected), num_columns)

        result_metadata = [dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS, i))['name'] for i in range(num_columns)]
        self.assertListEqual(result_metadata, expected)

        # validate dataframe
        dataframe = list(dataset.values())[0]
        self.assertListEqual(list(dataframe.columns.values), expected)

    def test_withconvert(self):
        # remove columns
        dataset = self._loadAndRemove()

        # convert to dataframe
        ds2df_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        ds2df_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=ds2df_hyperparams_class.defaults())
        dataframe = ds2df_primitive.produce(inputs=dataset).value

        # validate dataframe
        expected = ['d3mIndex', 'sepalWidth', 'petalWidth', 'species']

        num_columns = dataframe.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        self.assertEqual(len(expected), num_columns)
        result_metadata = [dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['name'] for i in range(num_columns)]
        self.assertListEqual(result_metadata, expected)

        self.assertListEqual(list(dataframe.columns.values), expected)

    def _loadAndRemove(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        dataset_metadata_before = dataset.metadata.to_json_structure()

        remove_columns_hyperparams_class = dataset_remove_columns.RemoveColumnsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        hp = remove_columns_hyperparams_class({
            'columns': (1, 3),
            'resource_id': '0'
        })
        remove_columns_primitive = dataset_remove_columns.RemoveColumnsPrimitive(hyperparams=hp)
        new_dataset = remove_columns_primitive.produce(inputs=dataset).value

        self.assertEqual(dataset.metadata.to_json_structure(), dataset_metadata_before)

        return new_dataset

    def test_can_accept(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        dataset_metadata_before = dataset.metadata.to_json_structure()

        dataset_metadata = dataset.metadata.set_for_value(None)

        remove_columns_hyperparams_class = dataset_remove_columns.RemoveColumnsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        hp = remove_columns_hyperparams_class({
            'columns': (1, 3),
            'resource_id': '0'
        })
        new_dataset_metadata = dataset_remove_columns.RemoveColumnsPrimitive.can_accept(method_name='produce', arguments={'inputs': dataset_metadata}, hyperparams=hp)

        self.assertTrue(new_dataset_metadata)

        # validate dataframe
        expected = ['d3mIndex', 'sepalWidth', 'petalWidth', 'species']

        num_columns = new_dataset_metadata.query(('0', metadata_base.ALL_ELEMENTS,))['dimension']['length']
        self.assertEqual(len(expected), num_columns)
        result_metadata = [new_dataset_metadata.query(('0', metadata_base.ALL_ELEMENTS, i))['name'] for i in range(num_columns)]
        self.assertListEqual(result_metadata, expected)

        self.assertEqual(dataset.metadata.to_json_structure(), dataset_metadata_before)


if __name__ == '__main__':
    unittest.main()
