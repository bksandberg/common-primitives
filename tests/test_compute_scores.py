import os
import unittest

import numpy

from d3m import container
from d3m.metadata import base as metadata_base

from common_primitives import compute_scores, utils


class ComputeScoresPrimitiveTestCase(unittest.TestCase):
    def test_regression(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        # We set semantic types like runtime would.
        dataset.metadata = dataset.metadata.add_semantic_type(('3', metadata_base.ALL_ELEMENTS, 4), 'https://metadata.datadrivendiscovery.org/types/Target')
        dataset.metadata = dataset.metadata.add_semantic_type(('3', metadata_base.ALL_ELEMENTS, 4), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')

        random = numpy.random.RandomState(42)

        # Create a synthetic prediction DataFrame.
        d3mIndex = dataset['3'].iloc[:, 0].astype(int)
        value = random.randn(len(d3mIndex))
        predictions = container.DataFrame({'d3mIndex': d3mIndex, 'value': value})
        shuffled_predictions = predictions.reindex(random.permutation(predictions.index)).reset_index(drop=True)

        hyperparams_class = compute_scores.ComputeScoresPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        metrics_class = hyperparams_class.configuration['metrics'].elements
        primitive = compute_scores.ComputeScoresPrimitive(hyperparams=hyperparams_class.defaults().replace({
            'metrics': [metrics_class({
                'metric': 'MEAN_SQUARED_ERROR',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'ROOT_MEAN_SQUARED_ERROR',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'ROOT_MEAN_SQUARED_ERROR_AVG',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'MEAN_ABSOLUTE_ERROR',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'R_SQUARED',
                'pos_label': None,
                'k': None,
            })],
        }))

        for name, pred in zip(['predictions', 'shuffled_predictions'], [predictions, shuffled_predictions]):
            scores = primitive.produce(inputs=pred, score_dataset=dataset).value
            self.assertEqual(scores.values.tolist(), [
                ['MEAN_SQUARED_ERROR', 'value', 3112.184932446708],
                ['ROOT_MEAN_SQUARED_ERROR', 'value', 55.786960236660214],
                ['ROOT_MEAN_SQUARED_ERROR_AVG', 'value', 55.786960236660214],
                ['MEAN_ABSOLUTE_ERROR', 'value', 54.579668078204385],
                ['R_SQUARED', 'value', -22.62418041588221],
            ], name)

            self.assertEqual(scores.metadata.query_column(0)['name'], 'metric', name)
            self.assertEqual(scores.metadata.query_column(1)['name'], 'targets', name)
            self.assertEqual(scores.metadata.query_column(2)['name'], 'value', name)

    def test_multivariate(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'multivariate_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        # We set semantic types like runtime would.
        dataset.metadata = dataset.metadata.add_semantic_type(('1', metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/Target')
        dataset.metadata = dataset.metadata.add_semantic_type(('1', metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        dataset.metadata = dataset.metadata.add_semantic_type(('1', metadata_base.ALL_ELEMENTS, 3), 'https://metadata.datadrivendiscovery.org/types/Target')
        dataset.metadata = dataset.metadata.add_semantic_type(('1', metadata_base.ALL_ELEMENTS, 3), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')

        random = numpy.random.RandomState(42)

        # Create a synthetic prediction DataFrame.
        d3mIndex = dataset['1'].iloc[:, 0].astype(int)
        amplitude = random.randn(len(d3mIndex))
        lengthscale = random.randn(len(d3mIndex))
        predictions = container.DataFrame({'d3mIndex': d3mIndex, 'amplitude': amplitude, 'lengthscale': lengthscale})
        shuffled_predictions = predictions.reindex(random.permutation(predictions.index)).reset_index(drop=True)

        hyperparams_class = compute_scores.ComputeScoresPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        metrics_class = hyperparams_class.configuration['metrics'].elements
        primitive = compute_scores.ComputeScoresPrimitive(hyperparams=hyperparams_class.defaults().replace({
            'metrics': [metrics_class({
                'metric': 'MEAN_SQUARED_ERROR',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'ROOT_MEAN_SQUARED_ERROR',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'ROOT_MEAN_SQUARED_ERROR_AVG',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'MEAN_ABSOLUTE_ERROR',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'R_SQUARED',
                'pos_label': None,
                'k': None,
            })],
        }))

        for name, pred in zip(['predictions', 'shuffled_predictions'], [predictions, shuffled_predictions]):
            scores = primitive.produce(inputs=pred, score_dataset=dataset).value
            self.assertEqual(scores.values.tolist(), [
                ['MEAN_SQUARED_ERROR', 'amplitude', 1.5134716754294575],
                ['MEAN_SQUARED_ERROR', 'lengthscale', 2.012102568475039],
                ['ROOT_MEAN_SQUARED_ERROR', 'amplitude', 1.2302323664371124],
                ['ROOT_MEAN_SQUARED_ERROR', 'lengthscale', 1.418486012787944],
                ['ROOT_MEAN_SQUARED_ERROR_AVG', 'amplitude,lengthscale', 1.3243591896125282],
                ['MEAN_ABSOLUTE_ERROR', 'amplitude', 0.8663878612550248],
                ['MEAN_ABSOLUTE_ERROR', 'lengthscale', 1.2198036763806932],
                ['R_SQUARED', 'amplitude', -1.575331100592595],
                ['R_SQUARED', 'lengthscale', -1.698729083171914],
            ], name)

            self.assertEqual(scores.metadata.query_column(0)['name'], 'metric', name)
            self.assertEqual(scores.metadata.query_column(1)['name'], 'targets', name)
            self.assertEqual(scores.metadata.query_column(2)['name'], 'value', name)

    def test_classification(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        # We set semantic types like runtime would.
        dataset.metadata = dataset.metadata.add_semantic_type(('0', metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/Target')
        dataset.metadata = dataset.metadata.add_semantic_type(('0', metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')

        random = numpy.random.RandomState(42)

        # Create a synthetic prediction DataFrame.
        d3mIndex = dataset['0'].iloc[:, 0].astype(int)
        species = random.choice(['Iris-setosa', 'Iris-versicolor', 'Iris-virginica'], len(d3mIndex))
        predictions = container.DataFrame({'d3mIndex': d3mIndex, 'species': species})
        shuffled_predictions = predictions.reindex(random.permutation(predictions.index)).reset_index(drop=True)

        hyperparams_class = compute_scores.ComputeScoresPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        metrics_class = hyperparams_class.configuration['metrics'].elements
        primitive = compute_scores.ComputeScoresPrimitive(hyperparams=hyperparams_class.defaults().replace({
            'metrics': [metrics_class({
                'metric': 'ACCURACY',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'F1_MICRO',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'F1_MACRO',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'ROC_AUC_MICRO',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'ROC_AUC_MACRO',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'JACCARD_SIMILARITY_SCORE',
                'pos_label': None,
                'k': None,
            }), metrics_class({
                'metric': 'PRECISION_AT_TOP_K',
                'pos_label': None,
                'k': 20,
            }), metrics_class({
                'metric': 'PRECISION_AT_TOP_K',
                'pos_label': None,
                'k': 10,
            })],
        }))

        for name, pred in zip(['predictions', 'shuffled_predictions'], [predictions, shuffled_predictions]):
            scores = primitive.produce(inputs=pred, score_dataset=dataset).value
            self.assertEqual(scores.values.tolist(), [
                ['ACCURACY', 'species', 0.4066666666666667],
                ['F1_MICRO', 'species', 0.4066666666666667],
                ['F1_MACRO', 'species', 0.4051068540623797],
                ['ROC_AUC_MICRO', 'species', 0.555],
                ['ROC_AUC_MACRO', 'species', 0.5549999999999999],
                ['JACCARD_SIMILARITY_SCORE', 'species', 0.4066666666666667],
                ['PRECISION_AT_TOP_K', 'species', 0.05],
                ['PRECISION_AT_TOP_K', 'species', 0.1],
            ], name)

            self.assertEqual(scores.metadata.query_column(0)['name'], 'metric', name)
            self.assertEqual(scores.metadata.query_column(1)['name'], 'targets', name)
            self.assertEqual(scores.metadata.query_column(2)['name'], 'value', name)

    def test_object_detection(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'object_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        # We set semantic types like runtime would.
        dataset.metadata = dataset.metadata.add_semantic_type(('1', metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/Target')
        dataset.metadata = dataset.metadata.add_semantic_type(('1', metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')

        random = numpy.random.RandomState(42)

        # Create a synthetic prediction DataFrame.
        predictions = container.DataFrame([
            [0, 'img_00285.png', '330,463,387,505', 0.0739],
            [1, 'img_00285.png', '420,433,451,498', 0.0910],
            [2, 'img_00285.png', '328,465,403,540', 0.1008],
            [3, 'img_00285.png', '480,477,508,522', 0.1012],
            [4, 'img_00285.png', '357,460,417,537', 0.1058],
            [5, 'img_00285.png', '356,456,391,521', 0.0843],
            [6, 'img_00225.png', '345,460,415,547', 0.0539],
            [7, 'img_00225.png', '381,362,455,513', 0.0542],
            [8, 'img_00225.png', '382,366,416,422', 0.0559],
            [9, 'img_00225.png', '730,463,763,583', 0.0588],
        ], columns=['d3mIndex', 'image', 'bounding_box', 'confidence'])
        shuffled_predictions = predictions.reindex(random.permutation(predictions.index)).reset_index(drop=True)

        hyperparams_class = compute_scores.ComputeScoresPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        metrics_class = hyperparams_class.configuration['metrics'].elements
        primitive = compute_scores.ComputeScoresPrimitive(hyperparams=hyperparams_class.defaults().replace({
            'metrics': [metrics_class({
                'metric': 'OBJECT_DETECTION_AVERAGE_PRECISION',
                'pos_label': None,
                'k': None,
            })],
        }))

        for name, pred in zip(['predictions', 'shuffled_predictions'], [predictions, shuffled_predictions]):
            scores = primitive.produce(inputs=pred, score_dataset=dataset).value
            self.assertEqual(scores.values.tolist(), [
                ['OBJECT_DETECTION_AVERAGE_PRECISION', 'bounding_box', 0.125],
            ], name)

        predictions = utils.remove_columns(predictions, [3])

        # We cannot test shuffling, because metric returns different values when there is on confidence.
        # See: https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/issues/118
        scores = primitive.produce(inputs=predictions, score_dataset=dataset).value
        self.assertEqual(scores.values.tolist(), [
            ['OBJECT_DETECTION_AVERAGE_PRECISION', 'bounding_box', 0.0625],
        ])

        self.assertEqual(scores.metadata.query_column(0)['name'], 'metric')
        self.assertEqual(scores.metadata.query_column(1)['name'], 'targets')
        self.assertEqual(scores.metadata.query_column(2)['name'], 'value')


if __name__ == '__main__':
    unittest.main()
