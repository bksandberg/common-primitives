import unittest

from d3m import container
from d3m.metadata import base

from common_primitives import utils


class TestUtils(unittest.TestCase):
    def test_copy_elements_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ())

        self.assertTrue(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1',), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['zlevel', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('zlevel', 'level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1', 'level2'))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_elements_metadata(metadata, target_metadata, ('level1', 'level2'), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z'},
        }, {
            'selector': ['zlevel', 'level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

    def test_copy_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ())

        self.assertTrue(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0': 'foobar0',
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'level1': 'foobar1',
                'level1b': '__NO_VALUE__',
                'level1a': 'foobar1a',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1',), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z', 'level1': 'foobar1', 'level1b': '__NO_VALUE__', 'level1a': 'foobar1a'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['zlevel', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('zlevel', 'level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1', 'level2'))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'level2': 'foobar2',
                'level2b': '__NO_VALUE__',
                'level2a': 'foobar2a',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = utils.copy_metadata(metadata, target_metadata, ('level1', 'level2'), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z', 'level2': 'foobar2', 'level2b': '__NO_VALUE__', 'level2a': 'foobar2a'},
        }, {
            'selector': ['zlevel', 'level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

    def test_select_columns(self):
        data = container.DataFrame({'a': [1, 2, 3], 'b': [4, 5, 6], 'c': [7, 8, 9]})
        data.metadata = data.metadata.update_column(0, {'name': 'aaa'})
        data.metadata = data.metadata.update_column(1, {'name': 'bbb'})
        data.metadata = data.metadata.update_column(2, {'name': 'ccc'})
        data.metadata = data.metadata.update((0, 0), {'row': '1'})
        data.metadata = data.metadata.update((1, 0), {'row': '2'})
        data.metadata = data.metadata.update((2, 0), {'row': '3'})
        data.metadata = data.metadata.update((0, base.ALL_ELEMENTS), {'all_elements_on_row': 'rowA'})

        data_metadata_before = data.metadata.to_json_structure()

        selected = utils.select_columns(data, [1, 0, 2, 1])

        self.assertIs(selected, selected.metadata.for_value)

        self.assertEqual(selected.values.tolist(), [[4, 1, 7, 4], [5, 2, 8, 5], [6, 3, 9, 6]])

        self.assertEqual(selected.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'bbb'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'aaa'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'ccc'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {'name': 'bbb'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 1],
            'metadata': {'row': '1'},
        }, {
            'selector': [1, 1],
            'metadata': {'row': '2'},
        }, {
            'selector': [2, 1],
            'metadata': {'row': '3'},
        }])

        self.assertEqual(data.metadata.to_json_structure(), data_metadata_before)

        selected = utils.select_columns(data, [1])

        self.assertIs(selected, selected.metadata.for_value)

        self.assertEqual(selected.values.tolist(), [[4], [5], [6]])

        self.assertEqual(selected.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 1,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'bbb'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }])

        self.assertEqual(data.metadata.to_json_structure(), data_metadata_before)

    def test_append_columns(self):
        left = container.DataFrame({'a1': [1, 2, 3], 'b1': [4, 5, 6], 'c1': [7, 8, 9]}, {
            'top_level': 'left',
        })
        left.metadata = left.metadata.update_column(0, {'name': 'aaa111'})
        left.metadata = left.metadata.update_column(1, {'name': 'bbb111'})
        left.metadata = left.metadata.update_column(2, {'name': 'ccc111'})
        left.metadata = left.metadata.update((0, 0), {'row': '1a'})
        left.metadata = left.metadata.update((1, 0), {'row': '2a'})
        left.metadata = left.metadata.update((2, 0), {'row': '3a'})
        left.metadata = left.metadata.update((0, base.ALL_ELEMENTS), {'all_elements_on_row': 'rowA'})

        right = container.DataFrame({'a2': [11, 12, 13], 'b2': [14, 15, 16], 'c2': [17, 18, 19]}, {
            'top_level': 'right',
        })
        right.metadata = right.metadata.update_column(0, {'name': 'aaa222'})
        right.metadata = right.metadata.update_column(1, {'name': 'bbb222'})
        right.metadata = right.metadata.update_column(2, {'name': 'ccc222'})
        right.metadata = right.metadata.update((0, 1), {'row': '1b'})
        right.metadata = right.metadata.update((1, 1), {'row': '2b'})
        right.metadata = right.metadata.update((2, 1), {'row': '3b'})
        right.metadata = right.metadata.update((0, base.ALL_ELEMENTS), {'all_elements_on_row': 'rowB'})

        right_metadata_before = right.metadata.to_json_structure()

        data = utils.append_columns(left, right, use_right_metadata=False)

        self.assertIs(data, data.metadata.for_value)

        self.assertEqual(data.values.tolist(), [[1, 4, 7, 11, 14, 17], [2, 5, 8, 12, 15, 18], [3, 6, 9, 13, 16, 19]])

        self.assertEqual(data.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'left',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 6,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'bbb111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'ccc111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {'name': 'aaa222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {'name': 'bbb222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {'name': 'ccc222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 0],
            'metadata': {'row': '1a'},
        }, {
            'selector': [0, 3],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 4],
            'metadata': {'row': '1b', 'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 5],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [1, 0],
            'metadata': {'row': '2a'},
        }, {
            'selector': [1, 4],
            'metadata': {'row': '2b'},
        }, {
            'selector': [2, 0],
            'metadata': {'row': '3a'},
        }, {
            'selector': [2, 4],
            'metadata': {'row': '3b'},
        }])

        data = utils.append_columns(left, right, use_right_metadata=True)

        self.assertIs(data, data.metadata.for_value)

        self.assertEqual(data.values.tolist(), [[1, 4, 7, 11, 14, 17], [2, 5, 8, 12, 15, 18], [3, 6, 9, 13, 16, 19]])

        self.assertEqual(data.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'right',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 6,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {'name': 'ccc222'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {'name': 'bbb222'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {'name': 'aaa222'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa111', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'bbb111', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'ccc111', 'structural_type': 'numpy.int64'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 4],
            'metadata': {'row': '1b'},
        }, {
            'selector': [0, 0],
            'metadata': {'row': '1a', 'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 1],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 2],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [1, 4],
            'metadata': {'row': '2b'},
        }, {
            'selector': [1, 0],
            'metadata': {'row': '2a'},
        }, {
            'selector': [2, 4],
            'metadata': {'row': '3b'},
        }, {
            'selector': [2, 0],
            'metadata': {'row': '3a'},
        }])

        self.assertEqual(right.metadata.to_json_structure(), right_metadata_before)

    def test_replace_columns(self):
        main = container.DataFrame({'a1': [1, 2, 3], 'b1': [4, 5, 6], 'c1': [7, 8, 9]}, {
            'top_level': 'main',
        })
        main.metadata = main.metadata.update_column(0, {'name': 'aaa111'})
        main.metadata = main.metadata.update_column(1, {'name': 'bbb111', 'extra': 'b_column'})
        main.metadata = main.metadata.update_column(2, {'name': 'ccc111'})
        main.metadata = main.metadata.update((0, 0), {'row': '1a'})
        main.metadata = main.metadata.update((1, 0), {'row': '2a'})
        main.metadata = main.metadata.update((2, 0), {'row': '3a'})
        main.metadata = main.metadata.update((0, base.ALL_ELEMENTS), {'all_elements_on_row': 'rowA'})

        main_metadata_before = main.metadata.to_json_structure()

        columns = container.DataFrame({'a2': [11, 12, 13], 'b2': [14, 15, 16]}, {
            'top_level': 'columns',
        })
        columns.metadata = columns.metadata.update_column(0, {'name': 'aaa222'})
        columns.metadata = columns.metadata.update_column(1, {'name': 'bbb222'})
        columns.metadata = columns.metadata.update((0, 1), {'row': '1b'})
        columns.metadata = columns.metadata.update((1, 1), {'row': '2b'})
        columns.metadata = columns.metadata.update((2, 1), {'row': '3b'})
        columns.metadata = columns.metadata.update((0, base.ALL_ELEMENTS), {'all_elements_on_row': 'rowB'})

        columns_metadata_before = columns.metadata.to_json_structure()

        new_main = utils.replace_columns(main, columns, [1, 2])

        self.assertIs(new_main, new_main.metadata.for_value)

        self.assertEqual(new_main.values.tolist(), [[1, 11, 14], [2, 12, 15], [3, 13, 16]])

        self.assertEqual(new_main.metadata.to_json_structure(), [{
            'selector': [], 'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'aaa222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'bbb222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 0],
            'metadata': {'row': '1a'},
        }, {
            'selector': [0, 1],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 2],
            'metadata': {'row': '1b', 'all_elements_on_row': 'rowB'},
        }, {
            'selector': [1, 0],
            'metadata': {'row': '2a'},
        }, {
            'selector': [1, 2],
            'metadata': {'row': '2b'},
        }, {
            'selector': [2, 0],
            'metadata': {'row': '3a'},
        }, {
            'selector': [2, 2],
            'metadata': {'row': '3b'},
        }])

        self.assertEqual(main_metadata_before, main.metadata.to_json_structure())
        self.assertEqual(columns_metadata_before, columns.metadata.to_json_structure())

        new_main = utils.replace_columns(main, columns, [0, 2])

        self.assertIs(new_main, new_main.metadata.for_value)

        self.assertEqual(new_main.values.tolist(), [[11, 4, 14], [12, 5, 15], [13, 6, 16]])

        self.assertEqual(new_main.metadata.to_json_structure(), [{
            'selector': [], 'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'bbb111',
                'extra': 'b_column',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'bbb222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 0],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 2],
            'metadata': {'row': '1b', 'all_elements_on_row': 'rowB'},
        }, {
            'selector': [1, 2],
            'metadata': {'row': '2b'},
        }, {
            'selector': [2, 2],
            'metadata': {'row': '3b'},
        }])

        self.assertEqual(main_metadata_before, main.metadata.to_json_structure())
        self.assertEqual(columns_metadata_before, columns.metadata.to_json_structure())

        new_main = utils.replace_columns(main, columns, [1])

        self.assertIs(new_main, new_main.metadata.for_value)

        self.assertEqual(new_main.values.tolist(), [[1, 11, 14, 7], [2, 12, 15, 8], [3, 13, 16, 9]])

        self.assertEqual(new_main.metadata.to_json_structure(), [{
            'selector': [], 'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa111'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'aaa222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {'name': 'bbb222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {'name': 'ccc111', 'structural_type': 'numpy.int64'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 0],
            'metadata': {'row': '1a'},
        }, {
            'selector': [0, 1],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 2],
            'metadata': {'row': '1b', 'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 3],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [1, 0],
            'metadata': {'row': '2a'},
        }, {
            'selector': [1, 2],
            'metadata': {'row': '2b'},
        }, {
            'selector': [2, 0],
            'metadata': {'row': '3a'},
        }, {
            'selector': [2, 2],
            'metadata': {'row': '3b'},
        }])

        self.assertEqual(main_metadata_before, main.metadata.to_json_structure())
        self.assertEqual(columns_metadata_before, columns.metadata.to_json_structure())

        new_main = utils.replace_columns(main, columns, [0, 1, 2])

        self.assertIs(new_main, new_main.metadata.for_value)

        self.assertEqual(new_main.values.tolist(), [[11, 14], [12, 15], [13, 16]])

        self.assertEqual(new_main.metadata.to_json_structure(), [{
            'selector': [], 'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'aaa222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'bbb222', 'structural_type': 'numpy.int64'},
        }, {
            'selector': [0, '__ALL_ELEMENTS__'],
            'metadata': {'all_elements_on_row': 'rowA'},
        }, {
            'selector': [0, 0],
            'metadata': {'all_elements_on_row': 'rowB'},
        }, {
            'selector': [0, 1],
            'metadata': {'row': '1b', 'all_elements_on_row': 'rowB'},
        }, {
            'selector': [1, 1],
            'metadata': {'row': '2b'},
        }, {
            'selector': [2, 1],
            'metadata': {'row': '3b'},
        }])

        self.assertEqual(main_metadata_before, main.metadata.to_json_structure())
        self.assertEqual(columns_metadata_before, columns.metadata.to_json_structure())

    def test_combine_columns(self):
        main = container.DataFrame({'a1': [1, 2, 3], 'b1': [4, 5, 6], 'c1': [7, 8, 9], 'd1': [10, 11, 12], 'e1': [13, 14, 15]}, {
            'top_level': 'main',
        })
        main.metadata = main.metadata.update_column(0, {'name': 'aaa111'})
        main.metadata = main.metadata.update_column(1, {'name': 'bbb111', 'extra': 'b_column'})
        main.metadata = main.metadata.update_column(2, {'name': 'ccc111'})

        columns2 = container.DataFrame({'a2': [21, 22, 23], 'b2': [24, 25, 26]}, {
            'top_level': 'columns2',
        })
        columns2.metadata = columns2.metadata.update_column(0, {'name': 'aaa222'})
        columns2.metadata = columns2.metadata.update_column(1, {'name': 'bbb222'})

        columns3 = container.DataFrame({'a3': [31, 32, 33], 'b3': [34, 35, 36]}, {
            'top_level': 'columns3',
        })
        columns3.metadata = columns3.metadata.update_column(0, {'name': 'aaa333'})
        columns3.metadata = columns3.metadata.update_column(1, {'name': 'bbb333'})

        result = utils.combine_columns('append', False, main, [1, 2], [columns2, columns3])

        self.assertIs(result.metadata.for_value, result)

        self.assertEqual(result.values.tolist(), [
            [1, 4, 7, 10, 13, 21, 24, 31, 34],
            [2, 5, 8, 11, 14, 22, 25, 32, 35],
            [3, 6, 9, 12, 15, 23, 26, 33, 36],
        ])

        self.assertEqual(result.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 9,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'bbb111',
                'extra': 'b_column',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'ccc111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'name': 'd1',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {
                'name': 'e1',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {
                'name': 'aaa222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 6],
            'metadata': {
                'name': 'bbb222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 7],
            'metadata': {
                'name': 'aaa333',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 8],
            'metadata': {
                'name': 'bbb333',
                'structural_type': 'numpy.int64',
            },
        }])

        result = utils.combine_columns('new', False, main, [1, 2], [columns2, columns3])

        self.assertIs(result.metadata.for_value, result)

        self.assertEqual(result.values.tolist(), [
            [21, 24, 31, 34],
            [22, 25, 32, 35],
            [23, 26, 33, 36],
        ])

        self.assertEqual(result.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'columns2',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'bbb222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'aaa333',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'name': 'bbb333',
                'structural_type': 'numpy.int64',
            },
        }])

        result = utils.combine_columns('replace', False, main, [1, 2], [columns2, columns3])

        self.assertIs(result.metadata.for_value, result)

        self.assertEqual(result.values.tolist(), [
            [1, 21, 24, 31, 34, 10, 13],
            [2, 22, 25, 32, 35, 11, 14],
            [3, 23, 26, 33, 36, 12, 15],
        ])

        self.assertEqual(result.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 7,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'aaa222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'bbb222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'name': 'aaa333',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {
                'name': 'bbb333',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {
                'name': 'd1',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 6],
            'metadata': {
                'name': 'e1',
                'structural_type': 'numpy.int64',
            },
        }])

        result = utils.combine_columns('replace', False, main, [0, 1, 2, 3, 4], [columns2, columns3])

        self.assertIs(result.metadata.for_value, result)

        self.assertEqual(result.values.tolist(), [
            [21, 24, 31, 34],
            [22, 25, 32, 35],
            [23, 26, 33, 36],
        ])

        self.assertEqual(result.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'bbb222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'aaa333',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'name': 'bbb333',
                'structural_type': 'numpy.int64',
            },
        }])

        result = utils.combine_columns('replace', False, main, [4], [columns2, columns3])

        self.assertIs(result.metadata.for_value, result)

        self.assertEqual(result.values.tolist(), [
            [1, 4, 7, 10, 21, 24, 31, 34],
            [2, 5, 8, 11, 22, 25, 32, 35],
            [3, 6, 9, 12, 23, 26, 33, 36],
        ])

        self.assertEqual(result.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 8,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'bbb111',
                'extra': 'b_column',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'ccc111',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'name': 'd1',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'aaa222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'bbb222',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 6],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'aaa333',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 7],
            'metadata': {
                'structural_type': 'numpy.int64',
                'name': 'bbb333',
            },
        }])

        result = utils.combine_columns('replace', False, main, [0, 2, 4], [columns2, columns3])

        self.assertIs(result.metadata.for_value, result)

        self.assertEqual(result.values.tolist(), [
            [21, 4, 24, 10, 31, 34],
            [22, 5, 25, 11, 32, 35],
            [23, 6, 26, 12, 33, 36],
        ])

        self.assertEqual(result.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'top_level': 'main',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 6,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'bbb111',
                'extra': 'b_column',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 3],
            'metadata': {
                'name': 'd1',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'aaa222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'bbb222',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 4],
            'metadata': {
                'name': 'aaa333',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 5],
            'metadata': {
                'name': 'bbb333',
                'structural_type': 'numpy.int64',
            },
        }])

    def test_get_index_columns(self):
        main = container.DataFrame({'a1': [1, 2, 3], 'b1': [4, 5, 6]})

        main.metadata = main.metadata.update((base.ALL_ELEMENTS, 0), {
            'name': 'image',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/PrimaryKey'],
        })
        main.metadata = main.metadata.update((base.ALL_ELEMENTS, 1), {
            'name': 'd3mIndex',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/PrimaryKey'],
        })

        self.assertEqual(utils.get_index_columns(main.metadata), [1, 0])


if __name__ == '__main__':
    unittest.main()
