
from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.base import (
    CallResult,
    GradientCompositionalityMixin,
    Gradients,
    DockerContainer,
)
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase

import common_primitives

from .utils import to_variable
from typing import Dict, Tuple
import os
import numpy as np  # type: ignore
import torch  # type: ignore
import torch.nn as nn  # type: ignore

Void = type(None)

Inputs = ndarray
Outputs = ndarray
Params = Void  # type: ignore


class Hyperparams(hyperparams.Hyperparams):
    loss_type = hyperparams.Enumeration[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['l1', 'mse', 'crossentropy'],
        default='mse',
        description='Type of end-to-end (pipeline) loss to be computed.'
    )
    target_inputs = hyperparams.Hyperparameter[ndarray](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=ndarray([0]),
        description='End-to-end (pipeline) training targets.'
                    ' When produce is called, the inputs to this primitive will be compared with the targets (target_inputs),'
                    ' and a loss measuring how dissimilar they are will be returned.'
    )


class Loss(GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
           TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive that produces an end-to-end (pipeline) loss, for the purpose of
    initiating backpropagation and fine-tuning (or training) a whole pipeline.
    This primitive is only needed during the end-to-end training of a pipeline and
    is not intended to be a part of the pipeline after the pipeline is trained.
    Forward pass See test_end_to_end.py in common_primitives repo for a running
    example of the setup. This loss primitive is conceptually the same as the
    Criterion module in Torch and PyTorch.
    """
    __author__ = 'Oxford DARPA D3M Team, Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
         'name': 'Loss primitive (i.e., a PyTorch Criterion) for end-to-end training of primitive chains inheriting from GradientCompositionalityMixin',
         'primitive_family': 'REGRESSION',
         'python_path': 'd3m.primitives.common_primitives.Loss',
         'source': {
            'name': common_primitives.__author__,
            'contact': 'mailto:gunes@robots.ox.ac.uk'
         },
         'version': '0.1.0',
         'id': 'f20ac610-1bf0-4401-b188-ef9dd8616a76',
         'installation': [{
             'type': metadata_module.PrimitiveInstallationType.PIP,
             'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                 git_commit=utils.current_git_commit(os.path.dirname(__file__)),
             ),
         }],
    })

    def __init__(self, *,
                 hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)
        loss_type = self.hyperparams['loss_type']
        if loss_type == 'l1':
            self._criterion = nn.L1Loss()
        elif loss_type == 'mse':
            self._criterion = nn.MSELoss()
        elif loss_type == 'crossentropy':
            self._criterion = nn.CrossEntropyLoss()
        else:
            raise ValueError('Unsupported loss: {}. Available options: l1, mse, crossentropy'.format(loss_type))
        self._actual_inputs = None  # type: torch.Variable
        self._target_inputs = to_variable(self.hyperparams['target_inputs'])
        self._loss = None  # type: torch.Variable

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        self._actual_inputs = to_variable(inputs, requires_grad=True)
        self._loss = self._criterion(self._actual_inputs, self._target_inputs)
        return CallResult(np.expand_dims(self._loss.data.numpy(), 0))

    def backward(self, *, gradient_outputs: Gradients[Outputs], fine_tune: bool = False, fine_tune_learning_rate: float = 0.00001,
                 fine_tune_weight_decay: float = 0.00001) -> Tuple[Inputs, Params]:  # type: ignore
        if self._loss is None:
            raise Exception('Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            if self._actual_inputs.grad is not None:
                self._actual_inputs.grad.data.zero_()
            self._loss.backward(gradient=to_variable(gradient_outputs))
            return self._actual_inputs.grad, None

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]:
        raise NotImplementedError()

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Params]:
        raise NotImplementedError()

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        raise NotImplementedError()
