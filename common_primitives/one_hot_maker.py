from numbers import Number
import os
import time
import typing
from typing import Any, Dict, List

import numpy as np  # type: ignore

from d3m.container.numpy import ndarray
from d3m.container.pandas import DataFrame
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer

import common_primitives
from .utils import denumpify

# dict where each value is a list containing an unknown data type
Inputs = DataFrame
# same as input but with each discrete data type converted to a one hot encoding and data vector for each key concatenated
Outputs = ndarray
# each key has a vector such that the encoding for vector[2] is [0 0 1 0 0 0 ...]


class Params(params.Params):
    categories: Dict[Any, List]


class Hyperparams(hyperparams.Hyperparams):
    cutoff_for_categorical = hyperparams.Uniform(
        default=0.05,
        lower=0,
        upper=1,
        description='The value used to guess if labels are discrete',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )


class OneHotMaker(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Attempts to detect discrete values in data and convert these to a
    one-hot embedding.
    """

    metadata = metadata_module.PrimitiveMetadata({
        'id': 'eaec420d-46eb-4ddf-a2cd-b8097345ff3e',
        'version': '0.2.0',
        'name': 'One-hot maker',
        'keywords': ['data processing', 'one-hot'],
        'source': {
            'name': common_primitives.__author__,
            'contact': 'mailto:willh@robots.ox.ac.uk'
        },
        'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'
                           .format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.common_primitives.OneHotMaker',
        'algorithm_types': ['ENCODE_ONE_HOT'],
        'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)

        self._cutoff_for_categorical = self.hyperparams['cutoff_for_categorical']
        self._training_input: typing.Dict = {}
        self._categories: typing.Dict = None

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        """
        All inputs should have the same keys - the lists for each key are then concatenated
        """
        inputs = {key: [denumpify(i) for i in inputs[key]] for key in inputs}  # type: ignore
        self._training_input = {}
        for key in inputs:
            base = self._training_input[key] if key in self._training_input else []
            self._training_input[key] = base + inputs[key]

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        start = time.time()
        if self._training_input is None:
            raise ValueError("Missing training data.")

        self._categories = {}
        for key in self._training_input:
            if timeout and time.time() > start + timeout:
                raise TimeoutError

            seen_values = []  # type: List
            for value in self._training_input[key]:
                if value not in seen_values:
                    seen_values.append(value)
            # try to infer whether or not the data is discrete
            if len(seen_values) > self._cutoff_for_categorical * len(self._training_input[key]) and \
                    all([isinstance(v, Number) for v in seen_values]):
                self._categories[key] = None
            else:
                self._categories[key] = seen_values

    def produce(self, *, inputs: Inputs,
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        inputs = {key: [denumpify(i) for i in inputs[key]] for key in inputs}  # type: ignore
        return CallResult(ndarray(np.array(
            self._produce_one(input=inputs, allow_unseen_categories=True)
        )))

    def _produce_one(self, *, input: dict, allow_unseen_categories: bool) -> ndarray:

        # convert discrete values to one-hot-vectors ...
        one_hot = {}
        for key in input:
            if self._categories[key] is None:
                one_hot[key] = input[key]
            else:
                one_hot[key] = [
                  self._make_onehot(self._categories[key], item, allow_unseen_categories) for item in input[key]
                ]

        # ... and convert the table to a list of rows
        result_vectors = []
        is_onehot = [self._categories[key] is not None for key in one_hot.keys()]
        for line in zip(*one_hot.values()):
            result: typing.List[typing.Any] = []
            for key, item in zip(is_onehot, line):
                if key:
                    result.extend(item)
                else:
                    result.append(item)
            result_vectors.append(result)

        return np.array(result_vectors)

    def _make_onehot(self, categories: List, item: str, allow_unseen_categories: bool) -> List[Any]:
        result = [0] * len(categories)
        try:
            result[categories.index(item)] = 1
        except ValueError:
            if item not in categories and not allow_unseen_categories:
                raise ValueError("Value {} not encountered during fit".format(item))
        return result

    def get_params(self) -> Params:
        return Params(
                    categories=self._categories)

    def set_params(self, *, params: Params) -> None:
        self._categories = params.categories
