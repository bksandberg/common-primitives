import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('NDArrayToListPrimitive',)

Inputs = container.ndarray
Outputs = container.List


class Hyperparams(hyperparams.Hyperparams):
    pass


class NDArrayToListPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which converts a numpy array into a list.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'b5101331-64b4-451a-beb6-260b40d1436b',
            'version': '0.1.0',
            'name': "ndarray to list converter",
            'python_path': 'd3m.primitives.data.NDArrayToList',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        return base.CallResult(container.List(inputs))

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        outputs_metadata = inputs_metadata.update((), {
            'structural_type': container.List,
        }, source=cls)

        # If input had more than 1 dimensions, then it was converted to a list with numpy arrays
        # as individual rows, so we have to update structural type at this level as well.
        if 'dimension' in outputs_metadata.query((metadata_base.ALL_ELEMENTS,)):
            outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS,), {
                'structural_type': container.ndarray,
            }, source=cls)

            # It could also be a table at that location.
            outputs_metadata = utils.set_table_metadata(outputs_metadata, at=(metadata_base.ALL_ELEMENTS,), source=cls)

        return outputs_metadata
