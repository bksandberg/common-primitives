import collections
import copy
import logging
import itertools
import math
import typing
from typing import *

import frozendict  # type: ignore
import torch  # type: ignore
import pandas  # type: ignore
from torch.autograd import Variable  # type: ignore
from pytypes import type_util  # type: ignore
import numpy as np  # type: ignore

from d3m import container, exceptions, utils as d3m_utils
from d3m.metadata import base as metadata_base

logger = logging.getLogger(__name__)


def add_dicts(dict1: typing.Dict, dict2: typing.Dict) -> typing.Dict:
    summation = {}
    for key in dict1:
        summation[key] = dict1[key] + dict2[key]
    return summation


def sum_dicts(dictArray: typing.Sequence[typing.Dict]) -> typing.Dict:
    assert len(dictArray) > 0
    summation = dictArray[0]
    for dictionary in dictArray:
        summation = add_dicts(summation, dictionary)
    return summation


def to_variable(value: Any, requires_grad: bool = False) -> Variable:
    """
    Converts an input to torch Variable object
    input
    -----
    value - Type: scalar, Variable object, torch.Tensor, numpy ndarray
    requires_grad  - Type: bool . If true then we require the gradient of that object

    output
    ------
    torch.autograd.variable.Variable object
    """

    if isinstance(value, Variable):
        return value
    elif torch.is_tensor(value):
        return Variable(value.float(), requires_grad=requires_grad)
    elif isinstance(value, np.ndarray) or isinstance(value, container.ndarray):
        return Variable(torch.from_numpy(value.astype(float)).float(), requires_grad=requires_grad)
    elif value is None:
        return None
    else:
        return Variable(torch.Tensor([float(value)]), requires_grad=requires_grad)


def to_tensor(value: Any) -> torch.FloatTensor:
    """
    Converts an input to a torch FloatTensor
    """
    if isinstance(value, np.ndarray):
        return torch.from_numpy(value).float()
    else:
        raise ValueError('Unsupported type: {}'.format(type(value)))


def refresh_node(node: Variable) -> Variable:
    return torch.autograd.Variable(node.data, True)


def log_mvn_likelihood(mean: torch.FloatTensor, covariance: torch.FloatTensor, observation: torch.FloatTensor) -> torch.FloatTensor:
    """
    all torch primitives
    all non-diagonal elements of covariance matrix are assumed to be zero
    """
    k = mean.shape[0]
    variances = covariance.diag()
    log_likelihood = 0
    for i in range(k):
        log_likelihood += - 0.5 * torch.log(variances[i]) \
                          - 0.5 * k * math.log(2 * math.pi) \
                          - 0.5 * ((observation[i] - mean[i])**2 / variances[i])
    return log_likelihood


def covariance(data: torch.FloatTensor) -> torch.FloatTensor:
    """
    input: NxD torch array
    output: DxD torch array

    calculates covariance matrix of input
    """

    N, D = data.size()
    cov = torch.zeros([D, D]).type(torch.DoubleTensor)
    for contribution in (torch.matmul(row.view(D, 1),
                         row.view(1, D))/N for row in data):
        cov += contribution
    return cov


def remove_mean(data: torch.FloatTensor) -> typing.Tuple[torch.FloatTensor, torch.FloatTensor]:
    """
    input: NxD torch array
    output: D-length mean vector, NxD torch array

    takes a torch tensor, calculates the mean of each
    column and subtracts it

    returns (mean, zero_mean_data)
    """

    N, D = data.size()
    mean = torch.zeros([D]).type(torch.DoubleTensor)
    for row in data:
        mean += row.view(D)/N
    zero_mean_data = data - mean.view(1, D).expand(N, D)
    return mean, zero_mean_data


def denumpify(unknown_object: typing.Any) -> typing.Any:
    """
    changes 'numpy.int's and 'numpy.float's etc to standard Python equivalents
    no effect on other data types
    """
    try:
        return unknown_object.item()
    except AttributeError:
        return unknown_object


# A copy of "Metadata._query" which starts ignoring "ALL_ELEMENTS" only at a certain depth.
# TODO: Make this part of internal metadata API.
def _query(selector: metadata_base.Selector, metadata_entry: typing.Optional[metadata_base.MetadataEntry], ignore_all_elements: int = None) -> frozendict.FrozenOrderedDict:
    if metadata_entry is None:
        return frozendict.FrozenOrderedDict()
    if len(selector) == 0:
        return metadata_entry.metadata

    segment, selector_rest = selector[0], selector[1:]

    if ignore_all_elements is not None:
        new_ignore_all_elements = ignore_all_elements - 1
    else:
        new_ignore_all_elements = None

    all_elements_metadata = _query(selector_rest, metadata_entry.all_elements, new_ignore_all_elements)
    if segment is metadata_base.ALL_ELEMENTS:
        metadata = all_elements_metadata
    elif segment in metadata_entry.elements:
        segment = typing.cast(metadata_base.SimpleSelectorSegment, segment)
        metadata = _query(selector_rest, metadata_entry.elements[segment], new_ignore_all_elements)
        if ignore_all_elements is None or ignore_all_elements > 0:
            metadata = metadata_base.Metadata()._merge_metadata(all_elements_metadata, metadata)
    elif ignore_all_elements is not None and ignore_all_elements <= 0:
        metadata = frozendict.FrozenOrderedDict()
    else:
        metadata = all_elements_metadata

    return metadata


def _copy_elements_metadata(source_metadata: metadata_base.Metadata, target_metadata: metadata_base.DataMetadata, from_selector: metadata_base.ListSelector,
                            to_selector: metadata_base.ListSelector, selector: metadata_base.ListSelector, ignore_all_elements: bool, check: bool, source: typing.Any) -> metadata_base.DataMetadata:
    # "ALL_ELEMENTS" is always first, if it exists, which works in our favor here.
    # We are copying metadata for both "ALL_ELEMENTS" and elements themselves, so
    # we do not have to merge metadata together for elements themselves.
    elements = source_metadata.get_elements(from_selector + selector)

    # TODO: Change that "update" below gets "check=check" argument once a newer version of core package than v2018.7.10 is released.
    #       This currently is just a workaround which temporarily unsets "for_value" making "update" not do the check.
    for_value = target_metadata.for_value
    try:
        if not check:
            target_metadata.for_value = None

        for element in elements:
            new_selector = selector + [element]
            metadata = _query(from_selector + new_selector, source_metadata._current_metadata, 0 if ignore_all_elements else len(from_selector))
            target_metadata = target_metadata.update(to_selector + new_selector, metadata, source=source)
            target_metadata = _copy_elements_metadata(source_metadata, target_metadata, from_selector, to_selector, new_selector, ignore_all_elements, check, source)

    finally:
        target_metadata.for_value = for_value

    return target_metadata


# TODO: Make this part of metadata API.
def copy_elements_metadata(source_metadata: metadata_base.Metadata, target_metadata: metadata_base.DataMetadata, from_selector: metadata_base.Selector,
                           to_selector: metadata_base.Selector = (), *, ignore_all_elements: bool = False, check: bool = True, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Recursively copies metadata of all elements of ``source_metadata`` to ``target_metadata``, starting at the
    ``from_selector`` and to a selector starting at ``to_selector``.
    It does not copy metadata at the ``from_selector`` itself.
    """

    return _copy_elements_metadata(source_metadata, target_metadata, list(from_selector), list(to_selector), [], ignore_all_elements, check, source)


# TODO: Make this part of metadata API.
def copy_metadata(source_metadata: metadata_base.Metadata, target_metadata: metadata_base.DataMetadata, from_selector: metadata_base.Selector,
                  to_selector: metadata_base.Selector = (), *, ignore_all_elements: bool = False, check: bool = True, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Recursively copies metadata of ``source_metadata`` to ``target_metadata``, starting at the
    ``from_selector`` and to a selector starting at ``to_selector``.
    """

    metadata = _query(from_selector, source_metadata._current_metadata, 0 if ignore_all_elements else len(from_selector))

    # Do not copy top-level "schema" field to a lower level.
    if from_selector == () and to_selector != () and 'schema' in metadata:
        # Copy so that we can mutate.
        metadata_dict = collections.OrderedDict(metadata)
        del metadata_dict['schema']
        metadata = frozendict.FrozenOrderedDict(metadata_dict)

    # TODO: Change that "update" below gets "check=check" argument once a newer version of core package than v2018.7.10 is released.
    #       This currently is just a workaround which temporarily unsets "for_value" making "update" not do the check.
    for_value = target_metadata.for_value
    try:
        if not check:
            target_metadata.for_value = None

        target_metadata = target_metadata.update(to_selector, metadata, source=source)

    finally:
        target_metadata.for_value = for_value

    return copy_elements_metadata(source_metadata, target_metadata, from_selector, to_selector, ignore_all_elements=ignore_all_elements, check=check, source=source)


def select_columns(inputs: container.DataFrame, columns: typing.Sequence[metadata_base.SimpleSelectorSegment], *,
                   source: typing.Any = None) -> container.DataFrame:
    """
    Given a DataFrame, it returns a new DataFrame with data and metadata only for given ``columns``.
    Moreover, columns are renumbered based on the position in ``columns`` list.
    Top-level metadata stays unchanged, except for updating the length of the columns dimension to
    the number of columns.

    So if the ``columns`` is ``[3, 6, 5]`` then output DataFrame will have three columns, ``[0, 1, 2]``,
    mapping data and metadata for columns ``3`` to ``0``, ``6`` to ``1`` and ``5`` to ``2``.

    This allows also duplication of columns.
    """

    if not columns:
        raise exceptions.InvalidArgumentValueError("No columns selected.")

    outputs = inputs.iloc[:, columns]
    outputs.metadata = select_columns_metadata(inputs.metadata, columns, source=source)
    outputs.metadata = outputs.metadata.set_for_value(outputs, generate_metadata=False, source=source)

    return outputs


# TODO: Make this part of metadata API.
def select_columns_metadata(inputs_metadata: metadata_base.DataMetadata, columns: typing.Sequence[metadata_base.SimpleSelectorSegment], *,
                            source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Given metadata, it returns a new metadata object with metadata only for given ``columns``.
    Moreover, columns are renumbered based on the position in ``columns`` list.
    Top-level metadata stays unchanged, except for updating the length of the columns dimension to
    the number of columns.

    So if the ``columns`` is ``[3, 6, 5]`` then output metadata will have three columns, ``[0, 1, 2]``,
    mapping metadata for columns ``3`` to ``0``, ``6`` to ``1`` and ``5`` to ``2``.

    This allows also duplication of columns.
    """

    if not columns:
        raise exceptions.InvalidArgumentValueError("No columns selected.")

    # This makes a copy so that we can modify metadata in-place.
    outputs_metadata = inputs_metadata.update(
        (metadata_base.ALL_ELEMENTS,),
        {
            'dimension': {
                'length': len(columns),
            },
        },
        source=source,
    )

    # TODO: Do this better. This change is missing an entry in metadata log.
    for element_metadata_entry in itertools.chain(
        [outputs_metadata._current_metadata.all_elements],
        outputs_metadata._current_metadata.elements.values(),
    ):
        if element_metadata_entry is None:
            continue

        elements = element_metadata_entry.elements
        element_metadata_entry.elements = {}
        for i, column_index in enumerate(columns):
            if column_index in elements:
                # If "column_index" is really numeric, we re-enumerate it.
                if isinstance(column_index, int):
                    element_metadata_entry.elements[i] = elements[column_index]
                else:
                    element_metadata_entry.elements[column_index] = elements[column_index]

    return outputs_metadata


# TODO: Make this part of metadata API.
def list_columns_with_semantic_types(metadata: metadata_base.DataMetadata, semantic_types: typing.Sequence[str], *,
                                     at: metadata_base.Selector = ()) -> typing.Sequence[int]:
    """
    This is similar to ``get_columns_with_semantic_type``, but it returns all column indices
    for a dimension instead of ``ALL_ELEMENTS`` element.

    Moreover, it operates on a list of semantic types, where a column is returned
    if it matches any semantic type on the list.
    """

    columns = []

    for element in metadata.get_elements(list(at) + [metadata_base.ALL_ELEMENTS]):
        metadata_semantic_types = metadata.query(list(at) + [metadata_base.ALL_ELEMENTS, element]).get('semantic_types', ())
        # TODO: Should we handle inheritance between semantic types here?
        if any(semantic_type in metadata_semantic_types for semantic_type in semantic_types):
            if element is metadata_base.ALL_ELEMENTS:
                return list(range(metadata.query(list(at) + [metadata_base.ALL_ELEMENTS]).get('dimension', {}).get('length', 0)))
            else:
                columns.append(typing.cast(int, element))

    return columns


def _matches_structural_type(metadata_structural_type: type, structural_type: typing.Union[str, type]) -> bool:
    if isinstance(structural_type, str):
        # TODO: Update to "d3m.utils.type_to_str" once released.
        return type_util.type_str(metadata_structural_type, assumed_globals={}, update_assumed_globals=False) == structural_type
    else:
        return d3m_utils.is_subclass(metadata_structural_type, structural_type)


def list_columns_with_structural_types(metadata: metadata_base.DataMetadata, structural_types: typing.Sequence[typing.Union[str, type]], *,
                                       at: metadata_base.Selector = ()) -> typing.Sequence[int]:
    """
    Returns a list of columns matching any of the structural types listed in
    ``structural_types``. Matching allows subclasses of those types.
    """

    columns = []

    for element in metadata.get_elements(list(at) + [metadata_base.ALL_ELEMENTS]):
        metadata_structural_type = metadata.query(list(at) + [metadata_base.ALL_ELEMENTS, element]).get('structural_type', None)

        if metadata_structural_type is None:
            continue

        if any(_matches_structural_type(metadata_structural_type, structural_type) for structural_type in structural_types):
            if element is metadata_base.ALL_ELEMENTS:
                return list(range(metadata.query(list(at) + [metadata_base.ALL_ELEMENTS]).get('dimension', {}).get('length', 0)))
            else:
                columns.append(typing.cast(int, element))

    return columns


def remove_columns(inputs: container.DataFrame, column_indices: typing.Sequence[int], *, source: typing.Any = None) -> container.DataFrame:
    """
    Removes columns from a given ``inputs`` DataFrame and returns one without, together with all
    metadata for columns removed as well.

    It throws an exception if no columns would be left after removing columns.
    """

    # We are not using "drop" because we are dropping by the column index (to support columns with same name).

    columns = list(range(inputs.shape[1]))

    if not columns:
        raise ValueError("No columns to remove.")

    for column_index in column_indices:
        columns.remove(column_index)

    if not columns:
        raise ValueError("Removing columns would have removed the last column.")

    output = inputs.iloc[:, columns]
    output.metadata = select_columns_metadata(inputs.metadata, columns, source=source)
    output.metadata = output.metadata.set_for_value(output, generate_metadata=False, source=source)

    return output


def remove_columns_metadata(inputs_metadata: metadata_base.DataMetadata, column_indices: typing.Sequence[int], *, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Analogous to ``remove_columns`` but operates only on metadata.
    """

    columns = list(range(inputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']))

    if not columns:
        raise ValueError("No columns to remove.")

    for column_index in column_indices:
        columns.remove(column_index)

    if not columns:
        raise ValueError("Removing columns would have removed the last column.")

    return select_columns_metadata(inputs_metadata, columns, source=source)


def append_columns(left: container.DataFrame, right: container.DataFrame, *, use_right_metadata: bool = False, source: typing.Any = None) -> container.DataFrame:
    """
    Appends all columns from ``right`` to the right of ``left``, together with all metadata.

    Top-level metadata of ``right`` is ignored, not merged, except if ``use_right_metadata``
    is set, in which case top-level metadata of ``left`` is ignored and one from ``right`` is
    used instead.
    """

    outputs = pandas.concat([left, right], axis=1)

    right_metadata = right.metadata
    if use_right_metadata:
        right_metadata = right_metadata.set_for_value(outputs, generate_metadata=False, source=source)
    else:
        outputs.metadata = left.metadata.set_for_value(outputs, generate_metadata=False, source=source)

    outputs.metadata = append_columns_metadata(outputs.metadata, right_metadata, use_right_metadata=use_right_metadata, source=source)

    return outputs


def append_columns_metadata(left_metadata: metadata_base.DataMetadata, right_metadata: metadata_base.DataMetadata, use_right_metadata: bool = False, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Appends metadata for all columns from ``right_metadata`` to the right of ``left_metadata``.

    Top-level metadata of ``right`` is ignored, not merged, except if ``use_right_metadata``
    is set, in which case top-level metadata of ``left`` is ignored and one from ``right`` is
    used instead.
    """

    left_length = left_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
    right_length = right_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

    if not use_right_metadata:
        outputs_metadata = left_metadata

        for column_index in range(right_length):
            # To go over "ALL_ELEMENTS" and all rows.
            for element in right_metadata.get_elements(()):
                outputs_metadata = copy_metadata(right_metadata, outputs_metadata, [element, metadata_base.ALL_ELEMENTS], [element, left_length + column_index], ignore_all_elements=True, check=False, source=source)
                outputs_metadata = copy_metadata(right_metadata, outputs_metadata, [element, column_index], [element, left_length + column_index], ignore_all_elements=True, check=False, source=source)

    else:
        # This makes a copy so that we can modify metadata in-place.
        outputs_metadata = right_metadata.update(
            (metadata_base.ALL_ELEMENTS,),
            {},
            source=source,
        )

        # TODO: Do this better. This change is missing an entry in metadata log.
        # Move columns and make space for left metadata to be prepended.
        # We iterate over a list so that we can change dict while iterating.
        for element_metadata_entry in itertools.chain(
            [outputs_metadata._current_metadata.all_elements],
            outputs_metadata._current_metadata.elements.values(),
        ):
            if element_metadata_entry is None:
                continue

            element_metadata_entry.elements = copy.copy(element_metadata_entry.elements)
            for element in sorted(element_metadata_entry.elements.keys(), reverse=True):
                metadata = element_metadata_entry.elements[element]
                del element_metadata_entry.elements[element]
                element_metadata_entry.elements[element + left_length] = metadata

        for column_index in range(left_length):
            # To go over "ALL_ELEMENTS" and all rows.
            for element in right_metadata.get_elements(()):
                outputs_metadata = copy_metadata(left_metadata, outputs_metadata, [element, metadata_base.ALL_ELEMENTS], [element, column_index], ignore_all_elements=True, check=False, source=source)
                outputs_metadata = copy_metadata(left_metadata, outputs_metadata, [element, column_index], [element, column_index], ignore_all_elements=True, check=False, source=source)

    outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS,), {'dimension': {'length': left_length + right_length}}, source=source)

    return outputs_metadata


def insert_columns(inputs: container.DataFrame, columns: container.DataFrame, at_column_index: int, *, source: typing.Any = None) -> container.DataFrame:
    """
    Inserts all columns from ``columns`` before ``at_column_index`` column in ``inputs``, pushing all
    existing columns to the right.

    E.g., ``at_column_index == 0`` means inserting ``columns`` at the beginning of ``inputs``.

    Top-level metadata of ``columns`` is ignored.
    """

    # TODO: This could probably be optimized without all the slicing and joining.

    columns_length = inputs.shape[1]

    if at_column_index < 0:
        raise exceptions.InvalidArgumentValueError("\"at_column_index\" is smaller than 0.")
    if at_column_index > columns_length:
        raise exceptions.InvalidArgumentValueError("\"at_column_index\" is larger than the range of existing columns.")

    if at_column_index == 0:
        return append_columns(columns, inputs, use_right_metadata=True, source=source)

    if at_column_index == columns_length:
        return append_columns(inputs, columns, source=source)

    before = select_columns(inputs, list(range(0, at_column_index)), source=source)
    after = select_columns(inputs, list(range(at_column_index, columns_length)), source=source)

    return append_columns(append_columns(before, columns, source=source), after, source=source)


def insert_columns_metadata(inputs_metadata: metadata_base.DataMetadata, columns_metadata: metadata_base.DataMetadata, at_column_index: int, *, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Analogous to ``insert_columns`` but operates only on metadata.
    """

    # TODO: This could probably be optimized without all the slicing and joining.

    columns_length = columns_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

    if at_column_index < 0:
        raise exceptions.InvalidArgumentValueError("\"at_column_index\" is smaller than 0.")
    if at_column_index > columns_length:
        raise exceptions.InvalidArgumentValueError("\"at_column_index\" is larger than the range of existing columns.")

    if at_column_index == 0:
        return append_columns_metadata(columns_metadata, inputs_metadata, use_right_metadata=True, source=source)

    if at_column_index == columns_length:
        return append_columns_metadata(inputs_metadata, columns_metadata, source=source)

    before = select_columns_metadata(inputs_metadata, list(range(0, at_column_index)), source=source)
    after = select_columns_metadata(inputs_metadata, list(range(at_column_index, columns_length)), source=source)

    return append_columns_metadata(append_columns_metadata(before, columns_metadata, source=source), after, source=source)


def _replace_column(inputs: container.DataFrame, inputs_column_index: int, columns: container.DataFrame,
                    columns_column_index: int, source: typing.Any) -> container.DataFrame:
    # We do not use "inputs.iloc[:, inputs_column_index] = columns.iloc[:, columns_column_index]"
    # but use the followig as a workaround.
    # See: https://github.com/pandas-dev/pandas/issues/22036
    # "inputs.iloc[:, [inputs_column_index]] = columns.iloc[:, [columns_column_index]]" does not work either.
    # See: https://github.com/pandas-dev/pandas/issues/22046
    inputs = pandas.concat([inputs.iloc[:, 0:inputs_column_index], columns.iloc[:, [columns_column_index]], inputs.iloc[:, inputs_column_index+1:]], axis=1)
    inputs.metadata = _replace_column_metadata(inputs.metadata, inputs_column_index, columns.metadata, columns_column_index, source)
    inputs.metadata = inputs.metadata.set_for_value(inputs, generate_metadata=False, source=source)
    return inputs


def _replace_column_metadata(inputs_metadata: metadata_base.DataMetadata, inputs_column_index: int, columns_metadata: metadata_base.DataMetadata,
                             columns_column_index: int, source: typing.Any) -> metadata_base.DataMetadata:
    outputs_metadata = inputs_metadata.remove_column(inputs_column_index, source=source)

    # To go over "ALL_ELEMENTS" and all rows.
    for element in columns_metadata.get_elements(()):
        outputs_metadata = copy_metadata(columns_metadata, outputs_metadata, [element, metadata_base.ALL_ELEMENTS], [element, inputs_column_index], ignore_all_elements=True, check=False, source=source)
        outputs_metadata = copy_metadata(columns_metadata, outputs_metadata, [element, columns_column_index], [element, inputs_column_index], ignore_all_elements=True, check=False, source=source)

    return outputs_metadata


def replace_columns(inputs: container.DataFrame, columns: container.DataFrame, column_indices: typing.List[int], *, copy: bool = True, source: typing.Any = None) -> container.DataFrame:
    """
    Replaces columns listed in ``column_indices`` with ``columns``, in order, in ``inputs``.

    ``column_indices`` and ``columns`` do not have to match in number of columns. Columns are first
    replaced in order for matching indices and columns. If then there are more ``column_indices`` than
    ``columns``, additional ``column_indices`` columns are removed. If there are more ``columns`` than
    ``column_indices`` columns, then additional ``columns`` are inserted after the last replaced column.

    If ``column_indices`` is empty, then the behavior is equivalent to calling ``append_columns``.

    Top-level metadata of ``columns`` is ignored.
    """

    # TODO: This could probably be optimized without all the slicing and joining.

    if not column_indices:
        return append_columns(inputs, columns, source=source)

    if copy:
        # We have to copy because "_replace" is modifying data in-place.
        # TODO: Replace with "copy.copy" to support DataFrame subclasses.
        #       We are currently using constructor to do the copy directly because we can disable metadata generation in this way.
        #       See: https://gitlab.com/datadrivendiscovery/common-primitives/issues/24
        outputs = container.DataFrame(inputs, generate_metadata=False, check=False, copy=True, source=source)
    else:
        outputs = inputs

    columns_length = columns.shape[1]
    columns_to_remove = []
    i = 0

    # This loop will run always at least once, so "column_index" will be set.
    while i < len(column_indices):
        column_index = column_indices[i]

        if i < columns_length:
            outputs = _replace_column(outputs, column_index, columns, i, source)
        else:
            # If there are more column indices than columns in "columns", we
            # select additional columns for removal.
            columns_to_remove.append(column_index)

        i += 1

    # When there are less column indices than columns in "columns", we insert the rest after
    # the last replaced column.
    if i < columns_length:
        columns = select_columns(columns, list(range(i, columns_length)), source=source)
        # "column_index" points to the last place we inserted a column, so "+ 1" points after it.
        outputs = insert_columns(outputs, columns, column_index + 1, source=source)

    # We remove columns at the end so that we do not break and column index used before.
    # When removing columns, column indices shift.
    if columns_to_remove:
        outputs = remove_columns(outputs, columns_to_remove, source=source)

    return outputs


def replace_columns_metadata(inputs_metadata: metadata_base.DataMetadata, columns_metadata: metadata_base.DataMetadata, column_indices: typing.List[int], *, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Analogous to ``replace_columns`` but operates only on metadata.
    """

    # TODO: This could probably be optimized without all the slicing and joining.

    if not column_indices:
        return append_columns_metadata(inputs_metadata, columns_metadata, source=source)

    outputs_metadata = inputs_metadata
    columns_length = columns_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
    columns_to_remove = []
    i = 0

    # This loop will run always at least once, so "column_index" will be set.
    while i < len(column_indices):
        column_index = column_indices[i]

        if i < columns_length:
            outputs_metadata = _replace_column_metadata(outputs_metadata, column_index, columns_metadata, i, source)
        else:
            # If there are more column indices than columns in "columns", we
            # select additional columns for removal.
            columns_to_remove.append(column_index)

        i += 1

    # When there are less column indices than columns in "columns", we insert the rest after
    # the last replaced column.
    if i < columns_length:
        columns_metadata = select_columns_metadata(columns_metadata, list(range(i, columns_length)), source=source)
        # "column_index" points to the last place we inserted a column, so "+ 1" points after it.
        outputs_metadata = insert_columns_metadata(outputs_metadata, columns_metadata, column_index + 1, source=source)

    # We remove columns at the end so that we do not break and column index used before.
    # When removing columns, column indices shift.
    if columns_to_remove:
        outputs_metadata = remove_columns_metadata(outputs_metadata, columns_to_remove, source=source)

    return outputs_metadata


def check_same_height(metadata1: metadata_base.DataMetadata, metadata2: metadata_base.DataMetadata) -> None:
    if metadata1.query(())['dimension']['length'] != metadata2.query(())['dimension']['length']:
        raise ValueError("Data does not match in the number of samples.")


def get_index_columns(metadata: metadata_base.DataMetadata, *, at: metadata_base.Selector = ()) -> typing.Sequence[int]:
    """
    Returns column indices of the primary index columns.

    It makes sure ``d3mIndex`` is always first listed.
    """

    index_columns = list_columns_with_semantic_types(metadata, ('https://metadata.datadrivendiscovery.org/types/PrimaryKey',), at=at)

    def d3m_index_first(index_column: int) -> int:
        if metadata.query((metadata_base.ALL_ELEMENTS, index_column)).get('name', None) == 'd3mIndex':
            return -1
        else:
            return 0

    return sorted(index_columns, key=d3m_index_first)


def _sort_right_indices(left: container.DataFrame, right: container.DataFrame, left_indices: typing.Sequence[int], right_indices: typing.Sequence[int]) -> container.DataFrame:
    # We try to handle different cases.

    # We do not do anything special. We assume both indices are the same.
    if len(left_indices) == 1 and len(right_indices) == 1:
        return right.set_index(right.iloc[:, right_indices[0]]).reindex(left.iloc[:, left_indices[0]]).reset_index(drop=True)

    left_index_names = [left.metadata.query_column(left_index).get('name', None) for left_index in left_indices]
    right_index_names = [right.metadata.query_column(right_index).get('name', None) for right_index in right_indices]

    left_index_series = [left.iloc[:, left_index] for left_index in left_indices]
    right_index_series = [right.iloc[:, right_index] for right_index in right_indices]

    # Number match, names match, order match, things look good.
    if left_index_names == right_index_names:
        # We know the length is larger than 1 because otherwise the first case would match.
        assert len(left_indices) > 1
        assert len(left_indices) == len(right_indices)

        return right.set_index(right_index_series).reindex(left_index_series).reset_index(drop=True)

    sorted_left_index_names = sorted(left_index_names)
    sorted_right_index_names = sorted(right_index_names)

    # Number and names match, but not the order.
    if sorted_left_index_names == sorted_right_index_names:
        # We know the length is larger than 1 because otherwise the first case would match.
        assert len(left_indices) > 1
        assert len(left_indices) == len(right_indices)

        # We sort index series to be in the sorted order based on index names.
        left_index_series = [s for _, s in sorted(zip(left_index_names, left_index_series), key=lambda pair: pair[0])]
        right_index_series = [s for _, s in sorted(zip(right_index_names, right_index_series), key=lambda pair: pair[0])]

        return right.set_index(right_index_series).reindex(left_index_series).reset_index(drop=True)

    if len(left_index_series) == len(right_index_series):
        # We know the length is larger than 1 because otherwise the first case would match.
        assert len(left_indices) > 1

        logger.warning("Primary indices both on left and right not have same names, but they do match in number.")

        return right.set_index(right_index_series).reindex(left_index_series).reset_index(drop=True)

    # It might be that there are duplicate columns on either or even both sides,
    # but that should be resolved by adding a primitive to remove duplicate columns first.
    raise ValueError("Left and right primary indices do not match in number.")


def horizontal_concat(left: container.DataFrame, right: container.DataFrame, *, use_index: bool = True,
                      remove_second_index: bool = True, use_right_metadata: bool = False, source: typing.Any = None) -> container.DataFrame:
    """
    Similar to ``append_columns``, but it respects primary index columns, by default.

    It has some heuristics how it tries to match up primary index columns in the case that there are
    multiple of them, but generally it aligns samples by all primary index columns.

    It is required that both inputs have the same number of samples.
    """

    check_same_height(left.metadata, right.metadata)

    left_indices = get_index_columns(left.metadata)
    right_indices = get_index_columns(right.metadata)

    if left_indices and right_indices:
        if use_index:
            old_right_metadata = right.metadata
            right = _sort_right_indices(left, right, left_indices, right_indices)
            # TODO: Reorder metadata rows as well.
            #       This should be relatively easy because we can just modify
            #       "right.metadata._current_metadata.metadata" map (and create a new action type for the log).
            right.metadata = old_right_metadata

        # Removing second primary key columns.
        if remove_second_index:
            right = remove_columns(right, right_indices, source=source)

    return append_columns(left, right, use_right_metadata=use_right_metadata, source=source)


def horizontal_concat_metadata(left_metadata: metadata_base.DataMetadata, right_metadata: metadata_base.DataMetadata, *, use_index: bool = True,
                               remove_second_index: bool = True, use_right_metadata: bool = False, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Similar to ``append_columns_metadata``, but it respects primary index columns, by default.

    It is required that both inputs have the same number of samples.
    """

    check_same_height(left_metadata, right_metadata)

    left_indices = get_index_columns(left_metadata)
    right_indices = get_index_columns(right_metadata)

    if left_indices and right_indices:
        if use_index:
            # TODO: Reorder metadata rows as well.
            #       We cannot really do this without metadata?
            pass

        # Removing second primary key column.
        if remove_second_index:
            right_metadata = remove_columns_metadata(right_metadata, right_indices, source=source)

    return append_columns_metadata(left_metadata, right_metadata, use_right_metadata=use_right_metadata, source=source)


def get_columns_to_use(metadata: metadata_base.DataMetadata, use_columns: typing.Sequence[int], exclude_columns: typing.Sequence[int],
                       can_use_column: typing.Callable) -> typing.Tuple[typing.List[int], typing.List[int]]:
    """
    A helper function which computes a list of columns to use and a list of columns to ignore
    given ``use_columns``, ``exclude_columns``, and a ``can_use_column`` function which should
    return ``True`` when column can be used.
    """

    all_columns = list(use_columns)

    # If "use_columns" is provided, this is our view of which columns exist.
    if not all_columns:
        # Otherwise, we start with all columns.
        all_columns = list(range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']))

        # And remove those in "exclude_columns".
        all_columns = [column_index for column_index in all_columns if column_index not in exclude_columns]

    # Now we create a list of columns for which "can_use_column" returns "True",
    # but also a list of columns for which it does not. The latter can be used
    # to determine if there is an error or warning. For example, when using "use_columns",
    # ideally, "columns_not_to_use" should be empty or a warning should be made.
    # Or, some primitives might require to operate on all columns, so "columns_not_to_use"
    # is empty, an error should be raised.
    columns_to_use = []
    columns_not_to_use = []
    for column_index in all_columns:
        if can_use_column(column_index):
            columns_to_use.append(column_index)
        else:
            columns_not_to_use.append(column_index)

    return columns_to_use, columns_not_to_use


def combine_columns(return_result: str, add_index_columns: bool, inputs: container.DataFrame, column_indices: typing.List[int],
                    columns_list: typing.List[container.DataFrame], *, source: typing.Any = None) -> container.DataFrame:
    """
    Method which appends existing columns, replaces them, or creates new result from them, based on
    ``return_result`` argument, which can be ``append``, ``replace``, or ``new``.

    ``add_index_columns`` controls if when creating a new result, primary index columns should be added
    if they are not already among columns.

    ``inputs`` is a DataFrame for which we are appending on replacing columns, or if we are creating new result,
    from where a primary index column can be taken.

    ``column_indices`` controls which columns in ``inputs`` were used to create ``columns_list``,
    and which columns should be replaced when replacing them.

    ``columns_list`` is a list of DataFrames representing all together new columns. The reason it is a list is
    to make it easier to operate per-column when preparing ``columns_list`` and not have to concat them all
    together unnecessarily.

    Top-level metadata in ``columns_list`` is ignored, except when creating new result.
    In that case top-level metadata from the first element in the list is used.

    When ``column_indices`` columns are being replaced with ``columns_list``, existing metadata in ``column_indices``
    columns is not preserved but replaced with metadata in ``columns_list``. Ideally, metadata for ``columns_list``
    has been constructed by copying source metadata from ``column_indices`` columns and modifying it as
    necessary to adapt it to new columns. But ``columns_list`` also can have completely new metadata, if this
    is more reasonable, but it should be understood that in this case when replacing ``column_indices``
    columns, any custom additional metadata on those columns will be lost.

    ``column_indices`` and ``columns_list`` do not have to match in number of columns. Columns are first
    replaced in order for matching indices and columns. If then there are more ``column_indices`` than
    ``columns_list``, additional ``column_indices`` columns are removed. If there are more ``columns_list`` than
    ``column_indices`` columns, then additional ``columns_list`` are inserted after the last replaced column.

    If ``column_indices`` is empty, then the replacing behavior is equivalent to appending.
    """

    if return_result == 'append':
        outputs = inputs
        for columns in columns_list:
            outputs = append_columns(outputs, columns, source=source)

    elif return_result == 'replace':
        if not column_indices:
            return combine_columns('append', add_index_columns, inputs, column_indices, columns_list, source=source)

        # We copy here and disable copying inside "replace_columns" to copy only once.
        # We have to copy because "replace_columns" is modifying data in-place.
        # TODO: Replace with "copy.copy" to support DataFrame subclasses.
        #       We are currently using constructor to do the copy directly because we can disable metadata generation in this way.
        #       See: https://gitlab.com/datadrivendiscovery/common-primitives/issues/24
        outputs = container.DataFrame(inputs, generate_metadata=False, check=False, copy=True, source=source)

        columns_replaced = 0
        for columns in columns_list:
            columns_length = columns.shape[1]
            if columns_replaced < len(column_indices):
                # It is OK if the slice of "column_indices" is shorter than "columns", Only those columns
                # listed in the slice will be replaced and others appended after the last replaced column.
                outputs = replace_columns(outputs, columns, column_indices[columns_replaced:columns_replaced + columns_length], copy=False, source=source)
            else:
                # We insert the rest of columns after the last columns we replaced. We know that "column_indices"
                # is non-empty and that the last item of "column_indices" points ot the last column we replaced
                # for those listed in "column_indices". We replaced more columns though, so we have to add the
                # difference, and then add 1 to insert after the last column.
                outputs = insert_columns(outputs, columns, column_indices[-1] + (columns_replaced - len(column_indices)) + 1, source=source)
            columns_replaced += columns_length

        if columns_replaced < len(column_indices):
            outputs = remove_columns(outputs, column_indices[columns_replaced:len(column_indices)], source=source)

    elif return_result == 'new':
        if not any(columns.shape[1] for columns in columns_list):
            raise ValueError("No columns produced.")

        outputs = columns_list[0]
        for columns in columns_list[1:]:
            outputs = append_columns(outputs, columns, source=source)

        if add_index_columns:
            index_columns = get_index_columns(inputs.metadata)
            if index_columns:
                # The first index column returned from "get_index_columns" is "d3mIndex" (if it exists),
                # and we want it to be the first column.
                if index_columns[0] not in column_indices:
                    d3m_index_column = 0
                    outputs = append_columns(select_columns(inputs, [index_columns[0]], source=source), outputs, use_right_metadata=True, source=source)
                else:
                    d3m_index_column = index_columns[0]
                other_index_columns = [index_column for index_column in index_columns[1:] if index_column not in column_indices]
                if other_index_columns:
                    # Other index columns are insert after the "d3mIndex" index column.
                    outputs = insert_columns(outputs, select_columns(inputs, other_index_columns, source=source), d3m_index_column + 1, source=source)

    else:
        raise exceptions.InvalidArgumentValueError("\"return_result\" has an invalid value: {return_result}".format(return_result=return_result))

    return outputs


def combine_columns_metadata(return_result: str, add_index_columns: bool, inputs_metadata: metadata_base.DataMetadata, column_indices: typing.List[int],
                             columns_metadata_list: typing.List[metadata_base.DataMetadata], *, source: typing.Any = None) -> metadata_base.DataMetadata:
    """
    Analogous to ``combine_columns`` but operates only on metadata.
    """

    if return_result == 'append':
        outputs_metadata = inputs_metadata
        for columns_metadata in columns_metadata_list:
            outputs_metadata = append_columns_metadata(outputs_metadata, columns_metadata, source=source)

    elif return_result == 'replace':
        if not column_indices:
            return combine_columns_metadata('append', add_index_columns, inputs_metadata, column_indices, columns_metadata_list, source=source)

        outputs_metadata = inputs_metadata
        columns_replaced = 0
        for columns_metadata in columns_metadata_list:
            columns_length = columns_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
            if columns_replaced < len(column_indices):
                # It is OK if the slice of "column_indices" is shorter than "columns", Only those columns
                # listed in the slice will be replaced and others appended after the last replaced column.
                outputs_metadata = replace_columns_metadata(outputs_metadata, columns_metadata, column_indices[columns_replaced:columns_replaced + columns_length], source=source)
            else:
                # We insert the rest of columns after the last columns we replaced. We know that "column_indices"
                # is non-empty and that the last item of "column_indices" points ot the last column we replaced
                # for those listed in "column_indices". We replaced more columns though, so we have to add the
                # difference, and then add 1 to insert after the last column.
                outputs_metadata = insert_columns_metadata(outputs_metadata, columns_metadata, column_indices[-1] + (columns_replaced - len(column_indices)) + 1, source=source)
            columns_replaced += columns_length

        if columns_replaced < len(column_indices):
            outputs_metadata = remove_columns_metadata(outputs_metadata, column_indices[columns_replaced:len(column_indices)], source=source)

    elif return_result == 'new':
        if not any(columns_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'] for columns_metadata in columns_metadata_list):
            raise ValueError("No columns produced.")

        outputs_metadata = columns_metadata_list[0]
        for columns_metadata in columns_metadata_list[1:]:
            outputs_metadata = append_columns_metadata(outputs_metadata, columns_metadata, source=source)

        if add_index_columns:
            index_columns = get_index_columns(inputs_metadata)
            if index_columns:
                # The first index column returned from "get_index_columns" is "d3mIndex" (if it exists),
                # and we want it to be the first column.
                if index_columns[0] not in column_indices:
                    d3m_index_column = 0
                    outputs_metadata = append_columns_metadata(select_columns_metadata(inputs_metadata, [index_columns[0]], source=source), outputs_metadata, use_right_metadata=True, source=source)
                else:
                    d3m_index_column = index_columns[0]
                other_index_columns = [index_column for index_column in index_columns[1:] if index_column not in column_indices]
                if other_index_columns:
                    # Other index columns are insert after the "d3mIndex" index column.
                    outputs_metadata = insert_columns_metadata(outputs_metadata, select_columns_metadata(inputs_metadata, other_index_columns, source=source), d3m_index_column + 1, source=source)

    else:
        raise exceptions.InvalidArgumentValueError("\"return_result\" has an invalid value: {return_result}".format(return_result=return_result))

    return outputs_metadata


def set_table_metadata(inputs_metadata: metadata_base.DataMetadata, *, at: metadata_base.Selector = (), source: typing.Any) -> metadata_base.DataMetadata:
    at = list(at)

    outputs_metadata = inputs_metadata

    # If input is at least 2D, then we set table metadata.
    if 'dimension' in outputs_metadata.query(at + [metadata_base.ALL_ELEMENTS]):
        metadata = outputs_metadata.query(at)

        semantic_types = list(metadata.get('semantic_types', []))
        if 'https://metadata.datadrivendiscovery.org/types/Table' not in semantic_types:
            semantic_types.append('https://metadata.datadrivendiscovery.org/types/Table')

        dimension_semantic_types = list(metadata.get('dimension', {}).get('semantic_types', []))
        if 'https://metadata.datadrivendiscovery.org/types/TabularRow' not in dimension_semantic_types:
            dimension_semantic_types.append('https://metadata.datadrivendiscovery.org/types/TabularRow')
        dimension_semantic_types = [semantic_type for semantic_type in dimension_semantic_types if semantic_type not in {'https://metadata.datadrivendiscovery.org/types/TabularColumn'}]

        outputs_metadata = outputs_metadata.update(at, {
            'dimension': {
                'name': 'rows',
                'semantic_types': dimension_semantic_types,
            },
            'semantic_types': semantic_types,
        }, source=source)

        metadata = outputs_metadata.query(at + [metadata_base.ALL_ELEMENTS])

        dimension_semantic_types = list(metadata.get('dimension', {}).get('semantic_types', []))
        if 'https://metadata.datadrivendiscovery.org/types/TabularColumn' not in dimension_semantic_types:
            dimension_semantic_types.append('https://metadata.datadrivendiscovery.org/types/TabularColumn')
        dimension_semantic_types = [semantic_type for semantic_type in dimension_semantic_types if semantic_type not in {'https://metadata.datadrivendiscovery.org/types/TabularRow'}]

        new_metadata: typing.Dict = {
            'dimension': {
                'name': 'columns',
                'semantic_types': dimension_semantic_types,
            },
        }

        if 'semantic_types' in metadata:
            new_metadata['semantic_types'] = [semantic_type for semantic_type in metadata['semantic_types'] if semantic_type not in {'https://metadata.datadrivendiscovery.org/types/Table'}]
            if not new_metadata['semantic_types']:
                new_metadata['semantic_types'] = metadata_base.NO_VALUE

        outputs_metadata = outputs_metadata.update(at + [metadata_base.ALL_ELEMENTS], new_metadata)

        selector: metadata_base.ListSelector = at + [metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS]
        while 'dimension' in outputs_metadata.query(selector):
            metadata = outputs_metadata.query(selector)

            new_metadata = {}

            if 'semantic_types' in metadata:
                new_metadata['semantic_types'] = [semantic_type for semantic_type in metadata['semantic_types'] if semantic_type not in {'https://metadata.datadrivendiscovery.org/types/Table'}]
                if not new_metadata['semantic_types']:
                    new_metadata['semantic_types'] = metadata_base.NO_VALUE

            if 'semantic_types' in metadata['dimension']:
                new_metadata['dimension'] = {}

                dimension_semantic_types = list(metadata['dimension']['semantic_types'])
                if 'https://metadata.datadrivendiscovery.org/types/TabularColumn' in dimension_semantic_types and metadata['dimension'].get('name', None) == 'columns':
                    new_metadata['dimension']['name'] = metadata_base.NO_VALUE
                if 'https://metadata.datadrivendiscovery.org/types/TabularRow' in dimension_semantic_types and metadata['dimension'].get('name', None) == 'rows':
                    new_metadata['dimension']['name'] = metadata_base.NO_VALUE

                dimension_semantic_types = [semantic_type for semantic_type in dimension_semantic_types if semantic_type not in {'https://metadata.datadrivendiscovery.org/types/TabularColumn', 'https://metadata.datadrivendiscovery.org/types/TabularRow'}]
                new_metadata['dimension']['semantic_types'] = dimension_semantic_types
                if not new_metadata['dimension']['semantic_types']:
                    new_metadata['dimension']['semantic_types'] = metadata_base.NO_VALUE

            if new_metadata:
                outputs_metadata = outputs_metadata.update(selector, new_metadata, source=source)

            selector.append(metadata_base.ALL_ELEMENTS)

    return outputs_metadata


# TODO: Make this part of internal metadata API.
def _merge_metadata_entries(metadata_entry1: metadata_base.MetadataEntry, metadata_entry2: metadata_base.MetadataEntry) -> metadata_base.MetadataEntry:
    """
    Merges ``metadata_entry2`` on top of ``metadata_entry1``, recursively, and
    returns a new metadata entry.
    """

    # A workaround to have accesss to metadata methods.
    # TODO: Remove this when moving to internal metadata API.
    self = metadata_base.Metadata()

    output_metadata_entry = metadata_base.MetadataEntry()

    # Merging elements.
    output_metadata_entry.elements = copy.copy(metadata_entry1.elements)
    for element_segment, element_metadata_entry in metadata_entry2.elements.items():
        if element_segment not in output_metadata_entry.elements:
            output_metadata_entry.elements[element_segment] = element_metadata_entry
        else:
            output_metadata_entry.elements[element_segment] = _merge_metadata_entries(output_metadata_entry.elements[element_segment], element_metadata_entry)

    # Merging "ALL_ELEMENTS".
    if metadata_entry1.all_elements is not None and metadata_entry2.all_elements is not None:
        output_metadata_entry.all_elements = _merge_metadata_entries(metadata_entry1.all_elements, metadata_entry2.all_elements)
    elif metadata_entry1.all_elements is not None:
        output_metadata_entry.all_elements = metadata_entry1.all_elements
    elif metadata_entry2.all_elements is not None:
        output_metadata_entry.all_elements = metadata_entry2.all_elements

    # Merging metadata:
    output_metadata_entry.metadata = self._merge_metadata(metadata_entry1.metadata, metadata_entry2.metadata)

    return output_metadata_entry


def get_column_index_from_column_name(inputs_metadata: metadata_base.DataMetadata, column_name: str, *, at: metadata_base.Selector = ()) -> int:
    for column_index in range(inputs_metadata.query(list(at) + [metadata_base.ALL_ELEMENTS,])['dimension']['length']):
        if inputs_metadata.query(list(at) + [metadata_base.ALL_ELEMENTS, column_index]).get('name', None) == column_name:
            return column_index

    raise ValueError(
        "Cannot resolve column name '{column_name}' at '{at}'.".format(
            column_name=column_name,
            at=at,
        ),
    )


def build_relation_graph(dataset: container.Dataset) -> typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]]:
    """
    Builds the relation graph from the ``dataset``.

    Each key in the output corresponds to a resource/table. The value under a key is the list of
    edges this table has. The edge is represented by a tuple of four elements. For example,
    if the edge is as follows: ``(resource_id, True, index_1, index_2, custom_state)``, this
    means that there is a foreign key that points to table ``resource_id``. Specifically,
    ``index_1`` column in the current table points to ``index_2`` column in the table ``resource_id``.

    Parameters
    ----------
    dataset : Dataset
        Dataset which consists of one or multiple tables.

    Returns
    -------
    Dict[str, List[Tuple[str, bool, int, int, Dict]]]
        Returns the relation graph in adjacency representation.
    """

    graph: typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]] = collections.defaultdict(list)

    for resource_id in dataset.keys():
        if not issubclass(dataset.metadata.query((resource_id,))['structural_type'], container.DataFrame):
            continue

        columns_length = dataset.metadata.query((resource_id, metadata_base.ALL_ELEMENTS,))['dimension']['length']
        for index in range(columns_length):
            column_metadata = dataset.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, index))

            if 'foreign_key' not in column_metadata:
                continue

            if column_metadata['foreign_key']['type'] != 'COLUMN':
                continue

            foreign_resource_id = column_metadata['foreign_key']['resource_id']

            # "COLUMN" foreign keys should not point to non-DataFrame resources.
            assert isinstance(dataset[foreign_resource_id], container.DataFrame), type(dataset[foreign_resource_id])

            if 'column_index' in column_metadata['foreign_key']:
                foreign_index = column_metadata['foreign_key']['column_index']
            elif 'column_name' in column_metadata['foreign_key']:
                foreign_index = get_column_index_from_column_name(dataset.metadata, column_metadata['foreign_key']['column_name'], at=foreign_resource_id)
            else:
                raise exceptions.UnexpectedValueError("Invalid foreign key: {foreign_key}".format(foreign_key=column_metadata['foreign_key']))

            # "True" and "False" implies forward and backward relationships, respectively.
            graph[resource_id].append((foreign_resource_id, True, index, foreign_index, {}))
            graph[foreign_resource_id].append((resource_id, False, foreign_index, index, {}))

    return graph


def get_tabular_resource(dataset: container.Dataset, resource_id: typing.Optional[str], *,
                         pick_entry_point: bool = True, pick_one: bool = True, has_hyperparameter: bool = True) -> typing.Tuple[str, container.DataFrame]:
    if resource_id is None and pick_entry_point:
        for dataset_resource_id in dataset.keys():
            if dataset.metadata.has_semantic_type((dataset_resource_id,), 'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint'):
                resource_id = dataset_resource_id
                break

    if resource_id is None and pick_one:
        tabular_resource_ids = [dataset_resource_id for dataset_resource_id, dataset_resource in dataset.items() if isinstance(dataset_resource, container.DataFrame)]
        if len(tabular_resource_ids) == 1:
            resource_id = tabular_resource_ids[0]

    if resource_id is None:
        if has_hyperparameter:
            if pick_entry_point and pick_one:
                raise ValueError("A Dataset with multiple tabular resources without an entry point and no resource specified as a hyper-parameter.")
            elif pick_entry_point:
                raise ValueError("A Dataset without an entry point and no resource specified as a hyper-parameter.")
            elif pick_one:
                raise ValueError("A Dataset with multiple tabular resources and no resource specified as a hyper-parameter.")
            else:
                raise ValueError("No resource specified as a hyper-parameter.")
        else:
            if pick_entry_point and pick_one:
                raise ValueError("A Dataset with multiple tabular resources without an entry point.")
            elif pick_entry_point:
                raise ValueError("A Dataset without an entry point.")
            elif pick_one:
                raise ValueError("A Dataset with multiple tabular resources.")
            else:
                raise ValueError("No resource specified.")

    else:
        resource = dataset[resource_id]

    if not isinstance(resource, container.DataFrame):
        raise TypeError("The Dataset resource \"{resource_id}\" is not a DataFrame, but \"{type}\".".format(
            resource_id=resource_id,
            type=type(resource),
        ))

    return resource_id, resource


def get_tabular_resource_metadata(dataset_metadata: metadata_base.DataMetadata, resource_id: typing.Optional[metadata_base.SelectorSegment], *,
                                  pick_entry_point: bool = True, pick_one: bool = True) -> metadata_base.SelectorSegment:
    if resource_id is None and pick_entry_point:
        # This can be also "ALL_ELEMENTS" and it will work out, but we prefer a direct resource ID,
        # if available. So we reverse the list, because the first is "ALL_ELEMENTS" if it exists.
        for dataset_resource_id in reversed(dataset_metadata.get_elements(())):
            if dataset_metadata.has_semantic_type((dataset_resource_id,), 'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint'):
                resource_id = dataset_resource_id
                break

    if resource_id is None and pick_one:
        # This can be also "ALL_ELEMENTS" and it will work out, but we prefer a direct resource ID,
        # if available. So we reverse the list, because the first is "ALL_ELEMENTS" if it exists.
        tabular_resource_ids = []
        for dataset_resource_id in reversed(dataset_metadata.get_elements(())):
            dataset_resource_type = dataset_metadata.query((resource_id,)).get('structural_type', None)

            if dataset_resource_type is None:
                continue

            if issubclass(dataset_resource_type, container.DataFrame):
                tabular_resource_ids.append(dataset_resource_id)

        if len(tabular_resource_ids) == 1:
            resource_id = tabular_resource_ids[0]

    if resource_id is None:
        if pick_entry_point and pick_one:
            raise ValueError("A Dataset with multiple tabular resources without an entry point and no DataFrame resource specified as a hyper-parameter.")
        elif pick_entry_point:
            raise ValueError("A Dataset without an entry point and no DataFrame resource specified as a hyper-parameter.")
        elif pick_one:
            raise ValueError("A Dataset with multiple tabular resources and no DataFrame resource specified as a hyper-parameter.")
        else:
            raise ValueError("No DataFrame resource specified as a hyper-parameter.")

    else:
        resource_type = dataset_metadata.query((resource_id,))['structural_type']

    if not issubclass(resource_type, container.DataFrame):
        raise TypeError("The Dataset resource \"{resource_id}\" is not a DataFrame, but \"{type}\".".format(
            resource_id=resource_id,
            type=resource_type,
        ))

    return resource_id
