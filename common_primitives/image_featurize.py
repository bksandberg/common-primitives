import os
import json
import numpy as np  # type: ignore
import typing
from typing import List, Dict, Union
from keras.applications import ResNet50, imagenet_utils  # type: ignore
from keras.models import Model  # type: ignore
from keras.layers import Dense  # type: ignore
import common_primitives
from d3m import utils
from d3m.container.numpy import ndarray
from d3m.container import DataFrame as d3m_dataframe
from d3m.metadata import hyperparams, params, base as metadata_module
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase


Inputs = ndarray
Outputs = d3m_dataframe


class Params(params.Params):
    layers: List[object]
    inputs: List[object]
    outputs: List[object]


class Hyperparams(hyperparams.Hyperparams):
    pass


class ImageTransferLearningTransformer(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Featurize images using Keras ResNet50 Application
    """

    __author__ = "Brown"
    metadata = metadata_module.PrimitiveMetadata({
        "algorithm_types": ['CLASSIFIER_CHAINS', 'RECURRENT_NEURAL_NETWORK', 'DEEP_NEURAL_NETWORK'],
        "name": "Keras ResNet50 Application",
        "primitive_family": metadata_module.PrimitiveFamily.FEATURE_EXTRACTION,
        "python_path": "d3m.primitives.common_primitives.ImageTransferLearningTransformer",
        "source": {
            'name': common_primitives.__author__,
        },
        "version": "0.1.0",
        "id": "8491c31d-6c5b-4996-96bc-1424ac9b6e07",
        'installation': [
            {
                'type': metadata_module.PrimitiveInstallationType.PIP,
                'package_uri':
                    'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                        git_commit=utils.current_git_commit(os.path.dirname(__file__)),),
            },
            {
                'type': 'FILE',
                'key': 'resnet50',
                'file_uri': "https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5",
                'file_digest': "bdc6c9f787f9f51dffd50d895f86e469cc0eb8ba95fd61f0801b1a264acb4819",
            },
        ],
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 volumes: typing.Dict[str, str] = None,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers, volumes=volumes)

        self._clf = None

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        from keras import backend as K  # type: ignore
        K.set_image_data_format('channels_first')  # type: ignore

        if self._clf is None:
            res_net = ResNet50(weights=None)
            if 'resnet50' in self.volumes and os.path.isfile(self.volumes['resnet50']):
                res_net.load_weights(filepath=self.volumes['resnet50'])
                res_net.layers.pop()

            in_ = res_net.input
            out_ = res_net.layers[-1].output
            self._clf = Model(in_, out_)

        images = imagenet_utils.preprocess_input(inputs)

        return CallResult(d3m_dataframe(self._clf.predict(images).reshape((images.shape[0], -1))))  # type: ignore
