import hashlib
import os
import typing

import dateutil.parser
import numpy  # type: ignore

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ColumnParserPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    parse_semantic_types = hyperparams.Set(
        elements=hyperparams.Enumeration(
            values=[
                'http://schema.org/Boolean', 'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'http://schema.org/Integer', 'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/FloatVector', 'http://schema.org/Time',
            ],
            # Default is ignored.
            # TODO: Remove default. See: https://gitlab.com/datadrivendiscovery/d3m/issues/141
            default='http://schema.org/Boolean',
        ),
        default=(
            'http://schema.org/Boolean', 'https://metadata.datadrivendiscovery.org/types/CategoricalData',
            'http://schema.org/Integer', 'http://schema.org/Float',
            'https://metadata.datadrivendiscovery.org/types/FloatVector', 'http://schema.org/Time',
        ),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of semantic types to parse. One can provide a subset of supported semantic types to limit what the primitive parses.",
    )
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='replace',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned?",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    parse_categorical_target_columns = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should it parse also categorical target columns?",
    )


class ColumnParserPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which parses strings into their parsed values.

    It goes over all columns (by default, controlled by ``use_columns``, ``exclude_columns``)
    and checks those with structural type ``str`` if they have a semantic type suggesting
    that they are a boolean value, categorical, integer, float, or time (by default,
    controlled by ``parse_semantic_types``). Categorical values are converted with
    hash encoding.

    What is returned is controlled by ``return_result`` and ``add_index_columns``.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'd510cb7a-1782-4f51-b44c-58f0236e47c7',
            'version': '0.4.0',
            'name': "Parses strings into their types",
            'python_path': 'd3m.primitives.data.ColumnParser',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        columns_to_use, output_columns = self._produce_columns(inputs, self.hyperparams, self)

        outputs = utils.combine_columns(self.hyperparams['return_result'], self.hyperparams['add_index_columns'], inputs, columns_to_use, output_columns, source=self)

        return base.CallResult(outputs)

    @classmethod
    def _can_use_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        # We produce only on columns which have not yet been parsed (are strings).
        if column_metadata['structural_type'] != str:
            return False

        semantic_types = column_metadata.get('semantic_types', [])

        for semantic_type in hyperparams['parse_semantic_types']:
            if semantic_type not in semantic_types:
                continue

            if semantic_type == 'https://metadata.datadrivendiscovery.org/types/CategoricalData':
                # Skip parsing if a column is categorical, but also a target column.
                if not hyperparams['parse_categorical_target_columns'] and 'https://metadata.datadrivendiscovery.org/types/Target' in semantic_types:
                    continue

            return True

        return False

    @classmethod
    def _produce_columns(cls, inputs: Outputs, hyperparams: Hyperparams, source: typing.Any) -> typing.Tuple[typing.List[int], typing.List[Outputs]]:
        columns_to_use = cls._get_columns(inputs.metadata, hyperparams)

        # We check against this list again, because there might be multiple matching semantic types
        # (which is not really valid).
        parse_semantic_types = hyperparams['parse_semantic_types']

        output_columns = []

        for column_index in columns_to_use:
            column_metadata = inputs.metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = column_metadata.get('semantic_types', [])
            if column_metadata['structural_type'] == str:
                if 'http://schema.org/Boolean' in parse_semantic_types and 'http://schema.org/Boolean' in semantic_types:
                    output_columns.append(cls._parse_boolean_data(inputs, column_index, source))

                elif 'https://metadata.datadrivendiscovery.org/types/CategoricalData' in parse_semantic_types and \
                        'https://metadata.datadrivendiscovery.org/types/CategoricalData' in semantic_types and \
                        (hyperparams['parse_categorical_target_columns'] or 'https://metadata.datadrivendiscovery.org/types/Target' not in semantic_types):
                    output_columns.append(cls._parse_categorical_data(inputs, column_index, source))

                elif 'http://schema.org/Integer' in parse_semantic_types and 'http://schema.org/Integer' in semantic_types:
                    # For primary key we know all values have to exist so we can assume they can always be represented as integers.
                    if 'https://metadata.datadrivendiscovery.org/types/PrimaryKey' in semantic_types:
                        integer_required = True
                    else:
                        integer_required = False

                    output_columns.append(cls._parse_integer(inputs, column_index, integer_required, source))

                elif 'http://schema.org/Float' in parse_semantic_types and 'http://schema.org/Float' in semantic_types:
                    output_columns.append(cls._parse_float_data(inputs, column_index, source))

                elif 'https://metadata.datadrivendiscovery.org/types/FloatVector' in parse_semantic_types and 'https://metadata.datadrivendiscovery.org/types/FloatVector' in semantic_types:
                    output_columns.append(cls._parse_float_vector_data(inputs, column_index, source))

                elif 'http://schema.org/Time' in parse_semantic_types and 'http://schema.org/Time' in semantic_types:
                    output_columns.append(cls._parse_time_data(inputs, column_index, source))

                else:
                    assert False, column_index

        assert len(output_columns) == len(columns_to_use)

        return columns_to_use, output_columns

    @classmethod
    def _produce_columns_metadata(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams,
                                  source: typing.Any) -> typing.Tuple[typing.List[int], typing.List[metadata_base.DataMetadata]]:
        columns_to_use = cls._get_columns(inputs_metadata, hyperparams)

        # We check against this list again, because there might be multiple matching semantic types
        # (which is not really valid).
        parse_semantic_types = hyperparams['parse_semantic_types']

        output_columns = []

        for column_index in columns_to_use:
            column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = column_metadata.get('semantic_types', [])
            if column_metadata['structural_type'] == str:
                if 'http://schema.org/Boolean' in parse_semantic_types and 'http://schema.org/Boolean' in semantic_types:
                    output_columns.append(cls._parse_boolean_metadata(inputs_metadata, column_index, source))

                elif 'https://metadata.datadrivendiscovery.org/types/CategoricalData' in parse_semantic_types and \
                        'https://metadata.datadrivendiscovery.org/types/CategoricalData' in semantic_types and \
                        (hyperparams['parse_categorical_target_columns'] or 'https://metadata.datadrivendiscovery.org/types/Target' not in semantic_types):
                    output_columns.append(cls._parse_categorical_metadata(inputs_metadata, column_index, source))

                elif 'http://schema.org/Integer' in parse_semantic_types and 'http://schema.org/Integer' in semantic_types:
                    output_columns.append(cls._parse_integer_metadata(inputs_metadata, column_index, source))

                elif 'http://schema.org/Float' in parse_semantic_types and 'http://schema.org/Float' in semantic_types:
                    output_columns.append(cls._parse_float_metadata(inputs_metadata, column_index, source))

                elif 'https://metadata.datadrivendiscovery.org/types/FloatVector' in parse_semantic_types and 'https://metadata.datadrivendiscovery.org/types/FloatVector' in semantic_types:
                    output_columns.append(cls._parse_float_vector_metadata(inputs_metadata, column_index, source))

                elif 'http://schema.org/Time' in parse_semantic_types and 'http://schema.org/Time' in semantic_types:
                    output_columns.append(cls._parse_time_metadata(inputs_metadata, column_index, source))

                else:
                    assert False, column_index

        assert len(output_columns) == len(columns_to_use)

        return columns_to_use, output_columns

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> typing.List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_column(inputs_metadata, column_index, hyperparams)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,  hyperparams['use_columns'], hyperparams['exclude_columns'], can_use_column)

        # We are OK if no columns ended up being parsed.
        # "utils.combine_columns" will throw an error if it cannot work with this.

        if hyperparams['use_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified columns can parsed. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _parse_boolean_data(cls, inputs: Outputs, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> Outputs:
        return cls._parse_categorical_data(inputs, column_index, source)

    @classmethod
    def _parse_boolean_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        return cls._parse_categorical_metadata(inputs_metadata, column_index, source)

    @classmethod
    def _parse_categorical_data(cls, inputs: Outputs, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> Outputs:
        values_map: typing.Dict[str, int] = {}
        for value in inputs.iloc[:, column_index]:
            value = value.strip()
            if value not in values_map:
                value_hash = hashlib.sha256(value.encode('utf8'))
                values_map[value] = int.from_bytes(value_hash.digest()[0:8], byteorder='little') ^ int.from_bytes(value_hash.digest()[8:16], byteorder='little') ^ \
                    int.from_bytes(value_hash.digest()[16:24], byteorder='little') ^ int.from_bytes(value_hash.digest()[24:32], byteorder='little')

        outputs = container.DataFrame({inputs.columns[column_index]: [values_map[value.strip()] for value in inputs.iloc[:, column_index]]}, generate_metadata=False)
        outputs.metadata = cls._parse_boolean_metadata(inputs.metadata, column_index, source)
        outputs.metadata = outputs.metadata.set_for_value(outputs, generate_metadata=False, source=source)

        return outputs

    @classmethod
    def _parse_categorical_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment,
                                    source: typing.Any) -> metadata_base.DataMetadata:
        outputs_metadata = utils.select_columns_metadata(inputs_metadata, [column_index], source=source)
        return outputs_metadata.update_column(0, {'structural_type': int}, source=source)

    @classmethod
    def _str_to_int(cls, value: str) -> typing.Union[float, int]:
        try:
            return int(value.strip())
        except ValueError:
            try:
                # Maybe it is an int represented as a float. Let's try this. This can get rid of non-integer
                # part of the value, but the integer was requested through a semantic type, so this is probably OK.
                return int(float(value.strip()))
            except ValueError:
                # No luck, use NaN to represent a missing value.
                return float('nan')

    @classmethod
    def _parse_integer(cls, inputs: Outputs, column_index: metadata_base.SimpleSelectorSegment,
                       integer_required: bool, source: typing.Any) -> container.DataFrame:
        outputs = container.DataFrame({inputs.columns[column_index]: [cls._str_to_int(value) for value in inputs.iloc[:, column_index]]}, generate_metadata=False)

        if outputs.dtypes.iloc[0].kind == 'f':
            structural_type: type = float
        elif outputs.dtypes.iloc[0].kind == 'i':
            structural_type = int
        else:
            assert False, outputs.dtypes.iloc[0]

        if structural_type is float and integer_required:
            raise ValueError("Not all values in a column can be parsed into integers, but only integers were expected.")

        outputs.metadata = utils.select_columns_metadata(inputs.metadata, [column_index], source=source)
        outputs.metadata = outputs.metadata.update_column(0, {'structural_type': structural_type}, source=source)
        outputs.metadata = outputs.metadata.set_for_value(outputs, generate_metadata=False, source=source)

        return outputs

    @classmethod
    def _parse_integer_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        outputs_metadata = utils.select_columns_metadata(inputs_metadata, [column_index], source=source)
        # Without data we assume we can parse everything into integers. This might not be true and
        # we might end up parsing into floats if we have to represent missing (or invalid) values.
        return outputs_metadata.update_column(0, {'structural_type': int}, source=source)

    @classmethod
    def _str_to_float(cls, value: str) -> float:
        try:
            return float(value.strip())
        except ValueError:
            return float('nan')

    @classmethod
    def _parse_float_data(cls, inputs: Outputs, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> Outputs:
        outputs = container.DataFrame({inputs.columns[column_index]: [cls._str_to_float(value) for value in inputs.iloc[:, column_index]]}, generate_metadata=False)
        outputs.metadata = cls._parse_float_metadata(inputs.metadata, column_index, source)
        outputs.metadata = outputs.metadata.set_for_value(outputs, generate_metadata=False, source=source)

        return outputs

    @classmethod
    def _parse_float_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        outputs_metadata = utils.select_columns_metadata(inputs_metadata, [column_index], source=source)
        return outputs_metadata.update_column(0, {'structural_type': float}, source=source)

    @classmethod
    def _parse_float_vector_data(cls, inputs: Outputs, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> Outputs:
        outputs = container.DataFrame({
            inputs.columns[column_index]: [
                container.ndarray([cls._str_to_float(value) for value in values.split(',')])
                for values in inputs.iloc[:, column_index]
            ],
        },
            generate_metadata=False,
        )
        outputs.metadata = cls._parse_float_metadata(inputs.metadata, column_index, source)
        # We have to automatically generate metadata to set ndarray dimension(s).
        outputs.metadata = outputs.metadata.set_for_value(outputs, generate_metadata=True, source=source)

        return outputs

    @classmethod
    def _parse_float_vector_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        outputs_metadata = utils.select_columns_metadata(inputs_metadata, [column_index], source=source)
        # We cannot know the dimension of the ndarray without data.
        outputs_metadata = outputs_metadata.update_column(0, {'structural_type': container.ndarray}, source=source)
        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS, 0, metadata_base.ALL_ELEMENTS), {'structural_type': numpy.float64}, source=source)
        return outputs_metadata

    @classmethod
    def _time_to_foat(cls, value: str) -> float:
        try:
            return dateutil.parser.parse(value).timestamp()
        except (ValueError, OverflowError):
            return float('nan')

    @classmethod
    def _parse_time_data(cls, inputs: Outputs, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> Outputs:
        outputs = container.DataFrame({inputs.columns[column_index]: [cls._time_to_foat(value) for value in inputs.iloc[:, column_index]]}, generate_metadata=False)
        outputs.metadata = cls._parse_time_metadata(inputs.metadata, column_index, source)
        outputs.metadata = outputs.metadata.set_for_value(outputs, generate_metadata=False, source=source)

        return outputs

    @classmethod
    def _parse_time_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        outputs_metadata = utils.select_columns_metadata(inputs_metadata, [column_index], source=source)
        return outputs_metadata.update_column(0, {'structural_type': float}, source=source)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        columns_to_use, output_columns = cls._produce_columns_metadata(inputs_metadata, hyperparams, cls)

        # We are stricter here than "produce" because we are not really useful.
        if not columns_to_use:
            return None

        return utils.combine_columns_metadata(hyperparams['return_result'], hyperparams['add_index_columns'], inputs_metadata, columns_to_use, output_columns, source=cls)
