import abc
import collections
import typing
import weakref

import frozendict  # type: ignore
import numpy  # type: ignore
import pandas  # type: ignore

from d3m import container, exceptions
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces import base, generator, transformer

from common_primitives import utils

__all__ = (
    'FileReaderPrimitiveBase',
    'DatasetSplitPrimitiveBase',
    'TabularSplitPrimitiveBase',
)

FileReaderInputs = container.DataFrame
FileReaderOutputs = container.DataFrame


class FileReaderHyperparams(hyperparams.Hyperparams):
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column does not contain filenames for supported media types, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='append',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should columns with read files be appended, should they replace original columns, or should only columns with read files be returned?",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


class FileReaderPrimitiveBase(transformer.TransformerPrimitiveBase[FileReaderInputs, FileReaderOutputs, FileReaderHyperparams]):
    """
    A primitive base class for reading files referenced in columns.
    """

    _supported_media_types: typing.Sequence[str] = ()
    _file_structural_type: type = None
    # If any of these semantic types already exists on a column, then nothing is done.
    # If all are missing, the first one is set.
    _file_semantic_types: typing.Sequence[str] = ()

    def __init__(self, *, hyperparams: FileReaderHyperparams) -> None:
        super().__init__(hyperparams=hyperparams)

        # Because same file can be referenced multiple times in multiple rows, we maintain
        # a cache of read files so that we do not have to read same files again and again.
        self._cache: weakref.WeakValueDictionary[typing.Tuple[int, str], typing.Any] = weakref.WeakValueDictionary()

    @classmethod
    def _can_use_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        if column_metadata['structural_type'] != str:
            return False

        semantic_types = column_metadata.get('semantic_types', [])
        media_types = set(column_metadata.get('media_types', []))

        if 'https://metadata.datadrivendiscovery.org/types/FileName' in semantic_types and media_types <= set(cls._supported_media_types):
            return True

        return False

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: FileReaderHyperparams) -> typing.List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,  hyperparams['use_columns'], hyperparams['exclude_columns'], can_use_column)

        # We are OK if no columns ended up being read.
        # "utils.combine_columns" will throw an error if it cannot work with this.

        if hyperparams['use_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified columns contain filenames for supported media types. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    def produce(self, *, inputs: FileReaderInputs, timeout: float = None, iterations: int = None) -> base.CallResult[FileReaderOutputs]:
        columns_to_use = self._get_columns(inputs.metadata, self.hyperparams)

        output_columns = [self._produce_column(inputs, column_index) for column_index in columns_to_use]

        outputs = utils.combine_columns(self.hyperparams['return_result'], self.hyperparams['add_index_columns'], inputs, columns_to_use, output_columns, source=self)

        if self.hyperparams['return_result'] == 'append':
            outputs.metadata = self._reassign_boundaries(outputs.metadata, columns_to_use, self)

        return base.CallResult(outputs)

    @abc.abstractmethod
    def _read_filepath(self, metadata: frozendict.FrozenOrderedDict, file_path: str) -> typing.Any:
        pass

    def _read_filename(self, column_index: int, metadata: frozendict.FrozenOrderedDict, file_name: str) -> typing.Any:
        file_path = metadata['location_base_uris'][0] + file_name

        # We do not use the structure where we check if the key exists in the cache and if not set it and then
        # return from the cache outside if clause because we are not sure garbage collection might not remove it
        # before we get to return. So we directly ask for a reference and return it, or we obtain the file
        # and populate the cache.
        file = self._cache.get((column_index, file_path), None)
        if file is not None:
            return file

        file = self._read_filepath(metadata, file_path)

        # We cache the file based on column index as well, because it could be that file is read differently
        # based on column metadata, or that resulting metadata is different for a different column.
        self._cache[(column_index, file_path)] = file

        return file

    def _produce_column(self, inputs: FileReaderInputs, column_index: int) -> FileReaderOutputs:
        read_files = [self._read_filename(column_index, inputs.metadata.query((row_index, column_index)), value) for row_index, value in enumerate(inputs.iloc[:, column_index])]

        column = container.DataFrame({inputs.columns[column_index]: read_files}, generate_metadata=False)

        column.metadata = self._produce_column_metadata(inputs.metadata, column_index, read_files, self)
        column.metadata = column.metadata.set_for_value(column, generate_metadata=True, source=self)

        return column

    @classmethod
    def _produce_column_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int,
                                 read_files: typing.Sequence[typing.Any], source: typing.Any) -> metadata_base.DataMetadata:
        # Unset "for_value" so that we do not trigger any validation error while modifying metadata.
        inputs_metadata = inputs_metadata.set_for_value(None, source=source)

        column_metadata = utils.select_columns_metadata(inputs_metadata, [column_index], source=source)
        column_metadata = column_metadata.update_column(0, {
            'structural_type': cls._file_structural_type,
            # Clear metadata useful for filename columns.
            'location_base_uris': metadata_base.NO_VALUE,
            'media_types': metadata_base.NO_VALUE,
        }, source=source)

        # It is not a filename anymore.
        column_metadata = column_metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/FileName', source=source)

        # At least one semantic type from listed semantic types should be set.
        semantic_types = column_metadata.query_column(0).get('semantic_types', [])
        if not set(semantic_types) & set(cls._file_semantic_types):
            # Add the first one.
            column_metadata = column_metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 0), cls._file_semantic_types[0], source=source)

        for row_index, file in enumerate(read_files):
            column_metadata = utils.copy_metadata(file.metadata, column_metadata, (), (row_index, 0), source=source)

        return column_metadata

    @classmethod
    def _reassign_boundaries(cls, inputs_metadata: metadata_base.DataMetadata, columns: typing.List[int], source: typing.Any) -> metadata_base.DataMetadata:
        """
        Moves metadata about boundaries from the filename column to image object column.
        """

        outputs_metadata = inputs_metadata
        columns_length = inputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        for column_index in range(columns_length):
            column_metadata = outputs_metadata.query_column(column_index)

            if 'boundary_for' not in column_metadata:
                continue

            # TODO: Support also "column_name" boundary metadata.
            if 'column_index' not in column_metadata['boundary_for']:
                continue

            try:
                i = columns.index(column_metadata['boundary_for']['column_index'])
            except ValueError:
                continue

            outputs_metadata = outputs_metadata.update_column(column_index, {
                'boundary_for': {
                    # We know that "columns" were appended at the end.
                    'column_index': columns_length - len(columns) + i,
                }
            }, source=source)

        return outputs_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: FileReaderHyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        columns_to_use = cls._get_columns(inputs_metadata, hyperparams)

        # We are stricter here than "produce" because we are not really useful.
        if not columns_to_use:
            return None

        output_columns = [cls._produce_column_metadata(inputs_metadata, column_index, [], cls) for column_index in columns_to_use]

        outputs_metadata = utils.combine_columns_metadata(hyperparams['return_result'], hyperparams['add_index_columns'], inputs_metadata, columns_to_use, output_columns, source=cls)

        if hyperparams['return_result'] == 'append':
            outputs_metadata = cls._reassign_boundaries(outputs_metadata, columns_to_use, cls)

        return outputs_metadata


DatasetSplitInputs = container.List
DatasetSplitOutputs = container.List


class DatasetSplitPrimitiveBase(generator.GeneratorPrimitiveBase[DatasetSplitOutputs, base.Params, base.Hyperparams]):
    """
    A base class for primitives which fit on a ``Dataset`` object to produce splits of that
    ``Dataset`` when producing. There are two produce methods: `produce` and `produce_score_data`.
    They take as an input a list of non-negative integers which identify which ``Dataset``
    splits to return.

    This class is parameterized using only by two type variables,
    ``Params`` and ``Hyperparams``.
    """

    @abc.abstractmethod
    def produce(self, *, inputs: DatasetSplitInputs, timeout: float = None, iterations: int = None) -> base.CallResult[DatasetSplitOutputs]:
        """
        For each input integer creates a ``Dataset`` split and produces the training ``Dataset`` object.
        This ``Dataset`` object should then be used to fit (train) the pipeline.
        """

    @abc.abstractmethod
    def produce_score_data(self, *, inputs: DatasetSplitInputs, timeout: float = None, iterations: int = None) -> base.CallResult[DatasetSplitOutputs]:
        """
        For each input integer creates a ``Dataset`` split and produces the scoring ``Dataset`` object.
        This ``Dataset`` object should then be used to test the pipeline and score the results.

        Output ``Dataset`` objects do not have targets redacted and are not directly suitable for testing.
        """

    @abc.abstractmethod
    def set_training_data(self, *, dataset: container.Dataset) -> None:  # type: ignore
        """
        Sets training data of this primitive, the ``Dataset`` to split.

        Parameters
        ----------
        dataset : Dataset
            The dataset to split.
        """


class TabularSplitPrimitiveParams(params.Params):
    dataset: typing.Optional[container.Dataset]
    main_resource_id: typing.Optional[str]
    splits: typing.Optional[typing.List[typing.Tuple[numpy.ndarray, numpy.ndarray]]]
    graph: typing.Optional[typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]]]


# TODO: Add "can_accept".
#       It should be relatively easy, metadata is almost the same as the input metadata,
#       input the number of rows in the dataset entry point might be lower.
# TODO: Make clear the assumption that both output container type (List) and output Datasets should have metadata.
#       Redaction primitive expects that, while there is officially no reason for Datasets
#       to really have metadata: metadata is stored available on the input container type, not
#       values inside it.
class TabularSplitPrimitiveBase(DatasetSplitPrimitiveBase[TabularSplitPrimitiveParams, base.Hyperparams]):
    def __init__(self, *, hyperparams: base.Hyperparams, random_seed: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        # We need random seed multiple times. So we create our own random state we use everywhere.
        self._random_state = numpy.random.RandomState(self.random_seed)
        self._fitted: bool = False
        self._dataset: container.Dataset = None
        self._main_resource_id: str = None
        self._splits: typing.List[typing.Tuple[numpy.ndarray, numpy.ndarray]] = None
        self._graph: typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]] = None

    def produce(self, *, inputs: DatasetSplitInputs, timeout: float = None, iterations: int = None) -> base.CallResult[DatasetSplitOutputs]:
        return self._produce(inputs, True)

    def produce_score_data(self, *, inputs: DatasetSplitInputs, timeout: float = None, iterations: int = None) -> base.CallResult[DatasetSplitOutputs]:
        return self._produce(inputs, False)

    def set_training_data(self, *, dataset: container.Dataset) -> None:  # type: ignore
        main_resource_id, main_resource = utils.get_tabular_resource(dataset, None, has_hyperparameter=False)

        self._main_resource_id = main_resource_id
        self._dataset = dataset
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> base.CallResult[None]:
        """
        This function computes everything in advance, including generating the relation graph.
        """

        if self._dataset is None:
            raise exceptions.InvalidStateError('Missing training data.')

        if self._fitted:
            return base.CallResult(None)

        targets, target_columns = self._get_target_columns(self._dataset, self._main_resource_id)
        attributes = self._get_attribute_columns(self._dataset, self._main_resource_id, target_columns)

        # Get splits' indices.
        self._splits = self._get_splits(attributes, targets)

        # Graph is the adjacency representation for the relations graph.
        self._graph = utils.build_relation_graph(self._dataset)

        self._fitted = True

        return base.CallResult(None)

    @abc.abstractmethod
    def _get_splits(self, attributes: pandas.DataFrame, targets: pandas.DataFrame) -> typing.List[typing.Tuple[numpy.ndarray, numpy.ndarray]]:
        pass

    @classmethod
    def _get_target_columns(cls, dataset: container.Dataset, main_resource_id: str) -> typing.Tuple[pandas.DataFrame, typing.Sequence[int]]:

        target_columns = utils.list_columns_with_semantic_types(dataset.metadata, ['https://metadata.datadrivendiscovery.org/types/TrueTarget'], at=(main_resource_id,))

        if not target_columns:
            raise ValueError("No target columns.")

        return dataset[main_resource_id].iloc[:, target_columns], target_columns

    @classmethod
    def _get_attribute_columns(cls, dataset: container.Dataset, main_resource_id: str, target_columns: typing.Sequence[int]) -> pandas.DataFrame:
        attribute_columns = utils.list_columns_with_semantic_types(dataset.metadata, ['https://metadata.datadrivendiscovery.org/types/Attribute'], at=(main_resource_id,))

        if not attribute_columns:
            # No attribute columns with semantic types, let's use all
            # non-target columns as attributes then.
            all_columns = list(range(dataset.metadata.query((main_resource_id, metadata_base.ALL_ELEMENTS,))['dimension']['length']))
            attribute_columns = [column_index for column_index in all_columns if column_index not in target_columns]

        if not attribute_columns:
            raise ValueError("No attribute columns.")

        return dataset[main_resource_id].iloc[:, attribute_columns]

    def _produce(self, inputs: DatasetSplitInputs, is_train: bool) -> base.CallResult[DatasetSplitOutputs]:
        """
        This function splits the fitted Dataset.

        Parameters
        ----------
        inputs : List[int]
            A list of 0-based indices which specify which splits to be used as test split in output.
        is_train : bool
            Whether we are producing train or test data.

        Returns
        -------
        List[Dataset]
            Returns a list of Datasets.
        """

        if not self._fitted:
            raise exceptions.InvalidStateError("Primitive not fitted.")

        output_datasets = container.List()

        for index in inputs:
            train_indices, test_indices = self._splits[index]

            # We store rows as sets, but later on we sort them when we cut DataFrames.
            row_indices_to_keep: typing.Dict[str, typing.Set[int]] = collections.defaultdict(set)
            if is_train:
                row_indices_to_keep[self._main_resource_id] = set(train_indices)
            else:
                row_indices_to_keep[self._main_resource_id] = set(test_indices)

            # If the hyper-parameter "delete_recursive" is set to "False", we do not populate "row_indices_to_keep"
            # with other rows in other tables, making "_cut_dataset" simply reuse them.
            if self.hyperparams['delete_recursive']:
                # We sort to be deterministic.
                for main_resource_row_index in sorted(row_indices_to_keep[self._main_resource_id]):
                    queue = []
                    queue.append((self._main_resource_id, [main_resource_row_index]))
                    while queue:
                        current_resource_id, current_row_indices = queue.pop(0)
                        current_resource = self._dataset[current_resource_id]

                        for edge_resource_id, edge_direction, edge_from_index, edge_to_index, custom_state in self._graph[current_resource_id]:
                            # All rows from the main resource we want are already there.
                            # TODO: What to do if we get a reference to the row in the main resource which is not part of this split?
                            #       This means that probably the split is invalid. We should not be generating such splits which do not
                            #       preserve reference loops and their consistency. Otherwise it is not really possible to denormalize
                            #       such Dataset properly: a reference is referencing a row in the main resource which does not exist.
                            if edge_resource_id == self._main_resource_id:
                                continue

                            edge_resource = self._dataset[edge_resource_id]

                            to_column_values = edge_resource.iloc[:, edge_to_index]
                            for from_column_value in current_resource.iloc[current_row_indices, edge_from_index]:
                                # We assume here that "index" corresponds to the default index with row indices.
                                rows_with_value = edge_resource.index[to_column_values == from_column_value]
                                # We sort to be deterministic.
                                new_rows_list = sorted(set(rows_with_value) - row_indices_to_keep[edge_resource_id])
                                row_indices_to_keep[edge_resource_id].update(new_rows_list)
                                queue.append((edge_resource_id, new_rows_list))

            output_datasets.append(self._cut_dataset(self._dataset, row_indices_to_keep))

        output_datasets.metadata = self._dataset.metadata.clear({
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.List,
            'dimension': {
                'length': len(output_datasets),
            },
        }, for_value=output_datasets, generate_metadata=False, source=self)

        # We update metadata based on metadata of each dataset.
        # TODO: In the future this might be done automatically by generate_metadata.
        #       See: https://gitlab.com/datadrivendiscovery/d3m/issues/119
        for index, dataset in enumerate(output_datasets):
            output_datasets.metadata = utils.copy_metadata(dataset.metadata, output_datasets.metadata, (), (index,), source=self)

        return base.CallResult(output_datasets)

    def _cut_dataset(self, dataset: container.Dataset, row_indices_to_keep: typing.Dict[str, typing.Set[int]]) -> container.Dataset:
        """
        Generate a new Dataset from the row indices for DataFrames.

        Parameters
        ----------
        dataset : Dataset
            Input Dataset.
        row_indices_to_keep : Dict[str, Set[int]]
            This is a dict where key is resource ID and value is a set of row indices to keep.
            If a resource ID is missing, the whole related resource should be kept.

        Returns
        -------
        Dataset
            Returns a new Dataset.
        """

        resources = {}
        metadata = dataset.metadata

        for resource_id, resource in dataset.items():
            # We keep any resource which is missing from "row_indices_to_keep".
            if resource_id not in row_indices_to_keep:
                resources[resource_id] = resource
            else:
                # "COLUMN" foreign keys should not point to non-DataFrame resources.
                assert isinstance(resource, container.DataFrame), type(resource)

                # We sort indices to get deterministic outputs from sets
                # (which do not have deterministic order).
                row_indices = sorted(row_indices_to_keep[resource_id])
                resources[resource_id] = dataset[resource_id].iloc[row_indices, :].reset_index(drop=True)

                # Change the metadata. Update the number of rows in the split.
                # This makes a copy so that we can modify metadata in-place.
                metadata = metadata.update(
                    (resource_id,),
                    {
                        'dimension': {
                            'length': len(row_indices),
                        },
                    },
                    source=self,
                )

                # TODO: Do this better. This change is missing an entry in metadata log.
                # Remove all rows not in this split and reorder those which are.
                for element_metadata_entry in [
                    metadata._current_metadata.all_elements,
                    metadata._current_metadata.elements[resource_id],
                ]:
                    if element_metadata_entry is None:
                        continue

                    elements = element_metadata_entry.elements
                    element_metadata_entry.elements = {}
                    for i, row_index in enumerate(row_indices):
                        if row_index in elements:
                            element_metadata_entry.elements[i] = elements[row_index]

        return container.Dataset(resources, metadata)

    def get_params(self) -> TabularSplitPrimitiveParams:
        if not self._fitted:
            return TabularSplitPrimitiveParams(
                dataset=None,
                main_resource_id=None,
                splits=None,
                graph=None,
            )

        return TabularSplitPrimitiveParams(
            dataset=self._dataset,
            main_resource_id=self._main_resource_id,
            splits=self._splits,
            graph=self._graph,
        )

    def set_params(self, *, params: TabularSplitPrimitiveParams) -> None:
        self._dataset = params['dataset']
        self._main_resource_id = params['main_resource_id']
        self._splits = params['splits']
        self._graph = params['graph']
        self._fitted = all(param is not None for param in params.values())

    def __getstate__(self) -> dict:
        state = super().__getstate__()

        state['random_state'] = self._random_state

        return state

    def __setstate__(self, state: dict) -> None:
        super().__setstate__(state)

        self._random_state = state['random_state']
