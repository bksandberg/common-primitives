import os
from collections import OrderedDict
from typing import cast, Any, Dict, List, Union, Sequence, Optional, Tuple

import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import sklearn.tree  # type: ignore
from sklearn.ensemble.forest import RandomForestClassifier  # type: ignore

from d3m import container, exceptions, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces.base import CallResult, ProbabilisticCompositionalityMixin, SamplingCompositionalityMixin, ContinueFitMixin
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

import common_primitives
from common_primitives import utils


Inputs = container.DataFrame
Outputs = container.DataFrame


class Params(params.Params):
    estimators: Optional[List[sklearn.tree.DecisionTreeClassifier]]
    classes: Optional[Union[np.ndarray, List[np.ndarray]]]
    n_classes: Optional[Union[int, List[int]]]
    n_features: Optional[int]
    n_outputs: Optional[int]
    target_columns_metadata: Optional[List[Dict]]
    oob_score: Optional[float]
    oob_decision_function: Optional[Union[np.ndarray, List[np.ndarray]]]


class Hyperparams(hyperparams.Hyperparams):
    # TODO: How to define it better?
    #       See: https://gitlab.com/datadrivendiscovery/d3m/issues/150
    n_estimators = hyperparams.UniformInt(
        lower=1,
        upper=10000,
        default=100,
        description='The number of trees in the forest.',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter',
            'https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter',
        ],
    )
    n_more_estimators = hyperparams.UniformInt(
        lower=1,
        upper=10000,
        default=100,
        description='When continuing a fit, it controls how many more trees to add every time.',
        semantic_types=[
            'https://metadata.datadrivendiscovery.org/types/TuningParameter',
            'https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter',
        ],
    )
    criterion = hyperparams.Enumeration[str](
        values=['gini', 'entropy'],
        default='gini',
        description='The function to measure the quality of a split.'
                    ' Supported criteria are "gini" for the Gini impurity and "entropy" for the information gain.'
                    ' Note: this parameter is tree-specific.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    max_features = hyperparams.Union[Union[int, float, str, None]](
        configuration=OrderedDict(
            # TODO: How to mark it as depending on the number of input features?
            fixed=hyperparams.Bounded[int](
                lower=1,
                upper=None,
                default=1,
                description='Consider "max_features" features at each split.'),
            ratio=hyperparams.Uniform(
                lower=0,
                upper=1,
                default=0.25,
                description='A percentage. "int(max_features * n_features)" features are considered at each split.',
            ),
            calculated=hyperparams.Enumeration[str](
                values=['sqrt', 'log2'],
                default='sqrt',
                description='If "sqrt", then "max_features = sqrt(n_features)". If "log2", then "max_features = log2(n_features)".',
            ),
            all_features=hyperparams.Hyperparameter[None](
                default=None,
                description='"max_features = n_features".',
            ),
        ),
        default='calculated',
        description='The number of features to consider when looking for the best split.'
                    ' The search for a split does not stop until at least one valid partition of the node samples is found,'
                    ' even if it requires to effectively inspect more than "max_features" features.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    max_depth = hyperparams.Union[Union[int, None]](
        configuration=OrderedDict(
            limit=hyperparams.Bounded[int](
                lower=0,
                upper=None,
                default=10,
            ),
            unlimited=hyperparams.Hyperparameter[None](
                default=None,
                description='Nodes are expanded until all leaves are pure or until all leaves contain less than "min_samples_split" samples.',
            ),
        ),
        default='unlimited',
        description='The maximum depth of the tree.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    min_samples_split = hyperparams.Union[Union[int, float]](
        configuration=OrderedDict(
            # TODO: How to mark it as depending on the number of input samples?
            fixed=hyperparams.Bounded[int](
                lower=2,
                upper=None,
                default=2,
                description='Consider "min_samples_split" as the minimum number.',
            ),
            ratio=hyperparams.Uniform(
                lower=0,
                upper=1,
                default=0.25,
                description='A percentage. "ceil(min_samples_split * n_samples)" are the minimum number of samples for each split.',
            ),
        ),
        default='fixed',
        description='The minimum number of samples required to split an internal node.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    min_samples_leaf = hyperparams.Union[Union[int, float]](
        configuration=OrderedDict(
            # TODO: How to mark it as depending on the number of input samples?
            fixed=hyperparams.Bounded[int](
                lower=1,
                upper=None,
                default=1,
                description='Consider "min_samples_leaf" as the minimum number.',
            ),
            ratio=hyperparams.Uniform(
                lower=0,
                upper=1,
                default=0.25,
                description='A percentage. "ceil(min_samples_leaf * n_samples)" are the minimum number of samples for each node.',
            ),
        ),
        default='fixed',
        description='The minimum number of samples required to be at a leaf node.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    min_weight_fraction_leaf = hyperparams.Uniform(
        lower=0,
        upper=1,
        default=0,
        description='The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    max_leaf_nodes = hyperparams.Union[Union[int, None]](
        configuration=OrderedDict(
            limit=hyperparams.Bounded[int](
                lower=0,
                upper=None,
                default=10,
            ),
            unlimited=hyperparams.Hyperparameter[None](
                default=None,
                description='Unlimited number of leaf nodes.',
            ),
        ),
        default='unlimited',
        description='Grow trees with "max_leaf_nodes" in best-first fashion. Best nodes are defined as relative reduction in impurity.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    min_impurity_decrease = hyperparams.Bounded[float](
        lower=0.0,
        upper=None,
        default=0.0,
        description='A node will be split if this split induces a decrease of the impurity greater than or equal to this value.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    bootstrap = hyperparams.Enumeration[str](
        values=['bootstrap', 'bootstrap_with_oob_score', 'disabled'],
        default='bootstrap',
        description='Whether bootstrap samples are used when building trees.'
                    ' And whether to use out-of-bag samples to estimate the generalization accuracy.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
    )
    # In reality values could also be -2 and so on, which would mean all CPUs minus 1,
    # but this does not really seem so useful here, so it is not exposed.
    n_jobs = hyperparams.Union[Union[int, None]](
        configuration=OrderedDict(
            limit=hyperparams.Bounded[int](
                lower=1,
                upper=None,
                default=1,
            ),
            all_cores=hyperparams.Hyperparameter[None](
                default=None,
                description='The number of jobs is set to the number of cores.',
            ),
        ),
        default='limit',
        description='The number of jobs to run in parallel for both "fit" and "produce".',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter'],
    )
    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to force primitive to operate on. If any specified column cannot be used, it is skipped.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of outputs column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        # Default value depends on the nature of the primitive.
        default='append',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should resulting columns be appended, should they replace original columns, or should only resulting columns be returned?",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


# TODO: Support weights on samples.
#       There is a "https://metadata.datadrivendiscovery.org/types/InstanceWeight" semantic type which should be used for this.
#       See: https://gitlab.com/datadrivendiscovery/d3m/issues/151
# TODO: How to use/determine class weights?
class RandomForestClassifierPrimitive(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                                      SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                                      ContinueFitMixin[Inputs, Outputs, Params, Hyperparams],
                                      SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A random forest classifier using ``sklearn.ensemble.forest.RandomForestClassifier``.

    It uses semantic types to determine which columns to operate on.
    """

    __author__ = 'Oxford DARPA D3M Team, Rob Zinkov <zinkov@robots.ox.ac.uk>'
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '37c2b19d-bdab-4a30-ba08-6be49edcc6af',
            'version': '0.2.0',
            'name': "Random forest classifier",
            'python_path': 'd3m.primitives.classifier.RandomForest',
            'keywords': ['random forest', 'decision tree'],
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:zinkov@robots.ox.ac.uk',
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.RANDOM_FOREST,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.CLASSIFICATION,
            'hyperparams_to_tune': [
                'max_leaf_nodes',
                'criterion',
                'max_features',
            ]
        }
    )

    def __init__(self, *, hyperparams: Hyperparams, random_seed: int = 0, _verbose: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        # We need random seed multiple times (every time an underlying "RandomForestClassifier" is instantiated),
        # and when we sample. So instead we create our own random state we use everywhere.
        self._random_state = np.random.RandomState(self.random_seed)
        self._verbose = _verbose
        self._training_inputs: Inputs = None
        self._training_outputs: Outputs = None
        self._new_training_data = False
        self._learner: RandomForestClassifier = None
        self._target_columns_metadata: List[Dict] = None

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        self._new_training_data = True

    def _create_learner(self) -> None:
        self._learner = RandomForestClassifier(
            n_estimators=self.hyperparams['n_estimators'],
            criterion=self.hyperparams['criterion'],
            max_features=self.hyperparams['max_features'],
            max_depth=self.hyperparams['max_depth'],
            min_samples_split=self.hyperparams['min_samples_split'],
            min_samples_leaf=self.hyperparams['min_samples_leaf'],
            min_weight_fraction_leaf=self.hyperparams['min_weight_fraction_leaf'],
            max_leaf_nodes=self.hyperparams['max_leaf_nodes'],
            min_impurity_decrease=self.hyperparams['min_impurity_decrease'],
            bootstrap=self.hyperparams['bootstrap'] in ['bootstrap', 'bootstrap_with_oob_score'],
            oob_score=self.hyperparams['bootstrap'] in ['bootstrap_with_oob_score'],
            n_jobs=-1 if self.hyperparams['n_jobs'] is None else self.hyperparams['n_jobs'],
            warm_start=True,
            random_state=self._random_state,
            verbose=self._verbose,
        )

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata) -> List[Dict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[Dict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = list(column_metadata.get('semantic_types', []))
            if 'https://metadata.datadrivendiscovery.org/types/PredictedTarget' not in semantic_types:
                semantic_types.append('https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            semantic_types = [semantic_type for semantic_type in semantic_types if semantic_type != 'https://metadata.datadrivendiscovery.org/types/TrueTarget']
            column_metadata['semantic_types'] = semantic_types

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata

    def _store_target_columns_metadata(self, outputs: Outputs) -> None:
        self._target_columns_metadata = self._get_target_columns_metadata(outputs.metadata)

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._training_inputs is None or self._training_outputs is None:
            raise exceptions.InvalidStateError("Missing training data.")

        # An optimization. Do not refit if data has not changed.
        if not self._new_training_data:
            return CallResult(None)
        self._new_training_data = False

        inputs, _ = self._select_inputs_columns(self._training_inputs)
        outputs, _ = self._select_outputs_columns(self._training_outputs)

        self._create_learner()

        # A special case for sklearn. It prefers an 1D array instead of 2D when there is only one target.
        if outputs.ndim == 2 and outputs.shape[1] == 1:
            fit_outputs = np.ravel(outputs)
        else:
            fit_outputs = outputs

        self._learner.fit(inputs, fit_outputs)

        self._store_target_columns_metadata(outputs)

        assert self._learner.n_outputs_ == len(self._target_columns_metadata), (self._learner.n_outputs_, len(self._target_columns_metadata))

        return CallResult(None)

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._training_inputs is None or self._training_outputs is None:
            raise exceptions.InvalidStateError("Missing training data.")

        # This model is not improving fitting if called multiple times on the same data.
        if not self._new_training_data:
            return CallResult(None)
        self._new_training_data = False

        if not self._learner:
            self._create_learner()

        n_estimators = self._learner.get_params()['n_estimators']
        n_estimators += self.hyperparams['n_more_estimators']
        self._learner.set_params(n_estimators=n_estimators)

        inputs, _ = self._select_inputs_columns(self._training_inputs)
        outputs, _ = self._select_outputs_columns(self._training_outputs)

        # A special case for sklearn. It prefers an 1D array instead of 2D when there is only one target.
        if outputs.ndim == 2 and outputs.shape[1] == 1:
            fit_outputs = np.ravel(outputs)
        else:
            fit_outputs = outputs

        self._learner.fit(inputs, fit_outputs)

        self._store_target_columns_metadata(outputs)

        assert self._learner.n_outputs_ == len(self._target_columns_metadata), (self._learner.n_outputs_, len(self._target_columns_metadata))

        return CallResult(None)

    @classmethod
    def _update_predictions_metadata(cls, inputs_metadata: metadata_base.DataMetadata, outputs: Optional[Outputs],
                                     target_columns_metadata: List[Dict], source: Any) -> metadata_base.DataMetadata:
        outputs_metadata = inputs_metadata.clear(for_value=outputs, generate_metadata=True, source=source)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            outputs_metadata = outputs_metadata.update_column(column_index, column_metadata, source=source)

        return outputs_metadata

    def _wrap_predictions(self, inputs: Inputs, predictions: np.ndarray) -> Outputs:
        outputs = container.DataFrame(predictions, generate_metadata=False, source=self)
        outputs.metadata = self._update_predictions_metadata(inputs.metadata, outputs, self._target_columns_metadata, self)
        return outputs

    def _predictions_from_proba(self, proba: np.ndarray) -> np.ndarray:
        """
        This is copied from ``ForestClassifier.predict``, but also includes a bugfix for
        `this issue`_.

        .. _this issue: https://github.com/scikit-learn/scikit-learn/issues/11451
        """

        if self._learner.n_outputs_ == 1:
            return self._learner.classes_.take(np.argmax(proba, axis=1), axis=0)

        else:
            predictions = []

            for k in range(self._learner.n_outputs_):
                predictions.append(self._learner.classes_[k].take(np.argmax(proba[k], axis=1), axis=0))

            return np.array(predictions).T

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if not self._learner:
            raise exceptions.InvalidStateError("Primitive not fitted.")

        selected_inputs, columns_to_use = self._select_inputs_columns(inputs)

        # We are not using "predict" directly because of a bug.
        # See: https://github.com/scikit-learn/scikit-learn/issues/11451
        proba = self._learner.predict_proba(selected_inputs)
        predictions = self._predictions_from_proba(proba)

        output_columns = [self._wrap_predictions(inputs, predictions)]

        outputs = utils.combine_columns(self.hyperparams['return_result'], self.hyperparams['add_index_columns'], inputs, columns_to_use, output_columns, source=self)

        return CallResult(outputs)

    def produce_feature_importances(self, *, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if not self._learner:
            raise exceptions.InvalidStateError("Primitive not fitted.")

        return CallResult(container.DataFrame(self._learner.feature_importances_))

    def sample(self, *, inputs: Inputs, num_samples: int = 1, timeout: float = None, iterations: int = None) -> CallResult[Sequence[Outputs]]:
        if not self._learner:
            raise exceptions.InvalidStateError("Primitive not fitted.")

        inputs, _ = self._select_inputs_columns(inputs)

        samples = []
        for i in range(num_samples):
            proba = self._random_state.choice(self._learner.estimators_).predict_proba(inputs)
            predictions = self._predictions_from_proba(proba)
            samples.append(self._wrap_predictions(inputs, predictions))

        return CallResult(samples)

    def log_likelihoods(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if not self._learner:
            raise exceptions.InvalidStateError("Primitive not fitted.")

        outputs, _ = self._select_outputs_columns(outputs)
        inputs, _ = self._select_inputs_columns(inputs)

        if outputs.shape[1] != self._learner.n_outputs_:
            raise exceptions.InvalidArgumentValueError("\"outputs\" argument does not have the correct number of target columns.")

        log_proba = self._learner.predict_log_proba(inputs)

        # Making it always a list, even when only one target.
        if self._learner.n_outputs_ == 1:
            log_proba = [log_proba]
            classes = [self._learner.classes_]
        else:
            classes = self._learner.classes_

        samples_length = inputs.shape[0]

        log_likelihoods = []
        for k in range(self._learner.n_outputs_):
            # We have to map each class to its internal (numerical) index used in the learner.
            # This allows "outputs" to contain string classes.
            outputs_column = outputs.iloc[:, k]
            classes_map = pd.Series(np.arange(len(classes[k])), index=classes[k])
            mapped_outputs_column = outputs_column.map(classes_map)

            # For each target column (column in "outputs"), for each sample (row) we pick the log
            # likelihood for a given class.
            log_likelihoods.append(log_proba[k][np.arange(samples_length), mapped_outputs_column])

        results = container.DataFrame(dict(enumerate(log_likelihoods)), generate_metadata=False, source=self)
        # TODO: Link new metadata to both "inputs" and "outputs" if/when this is possible.
        results.metadata = outputs.metadata.clear(for_value=results, generate_metadata=True, source=self)

        # TODO: Copy any other metadata?
        for k in range(self._learner.n_outputs_):
            column_metadata = outputs.metadata.query_column(k)
            if 'name' in column_metadata:
                results.metadata = results.metadata.update_column(k, {'name': column_metadata['name']}, source=self)

        return CallResult(results)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: Dict[str, Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments or 'outputs' not in arguments:
            return output_metadata

        inputs_metadata = cast(metadata_base.DataMetadata, arguments['inputs'])
        outputs_metadata = cast(metadata_base.DataMetadata, arguments['outputs'])

        inputs_columns_to_use = cls._get_inputs_columns(inputs_metadata, hyperparams)
        outputs_columns_to_use = cls._get_outputs_columns(outputs_metadata, hyperparams)

        selected_inputs_metadata = utils.select_columns_metadata(inputs_metadata, inputs_columns_to_use, source=cls)
        selected_outputs_metadata = utils.select_columns_metadata(outputs_metadata, outputs_columns_to_use, source=cls)

        target_columns_metadata = cls._get_target_columns_metadata(selected_outputs_metadata)

        output_column = cls._update_predictions_metadata(selected_inputs_metadata, None, target_columns_metadata, cls)

        output_column = output_column.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': Outputs,
            'dimension': {
                'length': selected_outputs_metadata.query(())['dimension']['length'],
            },
        })
        output_column = output_column.update((metadata_base.ALL_ELEMENTS,), {
            'dimension': {
                'length': selected_outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'],
            },
        })

        output_column = utils.set_table_metadata(output_column, source=cls)

        return utils.combine_columns_metadata(hyperparams['return_result'], hyperparams['add_index_columns'], inputs_metadata, inputs_columns_to_use, [output_column], source=cls)

    def get_params(self) -> Params:
        if not self._learner:
            return Params(
                estimators=None,
                classes=None,
                n_classes=None,
                n_features=None,
                n_outputs=None,
                target_columns_metadata=None,
                oob_score=None,
                oob_decision_function=None,
            )

        return Params(
            estimators=self._learner.estimators_,
            classes=self._learner.classes_,
            n_classes=self._learner.n_classes_,
            n_features=self._learner.n_features_,
            n_outputs=self._learner.n_outputs_,
            target_columns_metadata=self._target_columns_metadata,
            oob_score=getattr(self._learner, 'oob_score_', None),
            oob_decision_function=getattr(self._learner, 'oob_decision_function_', None),
        )

    def set_params(self, *, params: Params) -> None:
        if not all(params[param] is not None for param in ['estimators', 'classes', 'n_classes', 'n_features', 'n_outputs', 'target_columns_metadata']):
            self._learner = None
        else:
            self._create_learner()

            self._learner.estimators_ = params['estimators']
            self._learner.classes_ = params['classes']
            self._learner.n_classes_ = params['n_classes']
            self._learner.n_features_ = params['n_features']
            self._learner.n_outputs_ = params['n_outputs']
            self._target_columns_metadata = params['target_columns_metadata']
            if params['oob_score'] is not None:
                self._learner.oob_score_ = params['oob_score']
            if params['oob_decision_function'] is not None:
                self._learner.oob_decision_function_ = params['oob_decision_function']

            if self._learner.estimators_:
                # When continuing fitting, we are increasing "n_estimators", so we have to make sure
                # "n_estimators" matches the number of fitted estimators which might be different
                # from initial value set from through the hyper-parameter.
                self._learner.set_params(n_estimators=len(self._learner.estimators_))

    def __getstate__(self) -> dict:
        state = super().__getstate__()

        # Random state is not part of the "Params", but it is part of the state we want to
        # pickle and unpickle to have full reproducibility. So we have to add it ourselves here.
        # This is also difference between pickling/unpickling and "get_params"/"set_params".
        # The later saves only the model state which is useful to produce at a later time, but
        # if we want to also reproduce the exact sequence of values, we should be using pickling.
        state['random_state'] = self._random_state

        return state

    def __setstate__(self, state: dict) -> None:
        super().__setstate__(state)

        self._random_state = state['random_state']

    @classmethod
    def _can_use_inputs_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        return 'https://metadata.datadrivendiscovery.org/types/Attribute' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_inputs_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_inputs_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,  hyperparams['use_inputs_columns'], hyperparams['exclude_inputs_columns'], can_use_column)

        if not columns_to_use:
            raise ValueError("No inputs columns.")

        if hyperparams['use_inputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified inputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _can_use_outputs_column(cls, outputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = outputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        return 'https://metadata.datadrivendiscovery.org/types/TrueTarget' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_outputs_columns(cls, outputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_outputs_column(outputs_metadata, column_index)

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(outputs_metadata, hyperparams['use_outputs_columns'], hyperparams['exclude_outputs_columns'], can_use_column)

        if not columns_to_use:
            raise ValueError("No outputs columns.")

        if hyperparams['use_outputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified outputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    def _select_inputs_columns(self, inputs: Inputs) -> Tuple[Inputs, List[int]]:
        columns_to_use = self._get_inputs_columns(inputs.metadata, self.hyperparams)

        return utils.select_columns(inputs, columns_to_use, source=self), columns_to_use

    def _select_outputs_columns(self, outputs: Outputs) -> Tuple[Outputs, List[int]]:
        columns_to_use = self._get_outputs_columns(outputs.metadata, self.hyperparams)

        return utils.select_columns(outputs, columns_to_use, source=self), columns_to_use
