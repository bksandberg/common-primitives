import copy
import itertools
import typing
import os

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('RemoveDuplicateColumnsPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column is not a duplicate with any other specified, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )


# TODO: Compare columns also by determining if there exists a bijection between two columns and find such columns duplicate as well.
class RemoveDuplicateColumnsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which removes duplicate columns based on exact match in all their values.

    It adds names of removed columns into ``other_names`` metadata for columns remaining.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '130513b9-09ca-4785-b386-37ab31d0cf8b',
            'version': '0.1.0',
            'name': "Removes duplicate columns",
            'python_path': 'd3m.primitives.data.RemoveDuplicateColumns',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        columns_to_use = self._get_columns(inputs.metadata, self.hyperparams)
        columns_to_use_length = len(columns_to_use)

        equal_columns = []
        for i in range(columns_to_use_length):
            for j in range(i + 1, columns_to_use_length):
                if inputs.iloc[:, columns_to_use[i]].equals(inputs.iloc[:, columns_to_use[j]]):
                    equal_columns.append((i, j))

        # It might be that more columns are equal to each other, so we resolve those and
        # will keep only the first column and remove all others.
        equal_columns_map: typing.Dict[int, typing.Set[int]] = {}
        for i, j in equal_columns:
            for first, others in equal_columns_map.items():
                if first == i:
                    others.add(j)
                    break
                elif i in others:
                    others.add(j)
                    break
            else:
                equal_columns_map[i] = {j}

        outputs = copy.copy(inputs)

        # Set "other_names" metadata on columns remaining.
        for first, others in equal_columns_map.items():
            first_name = outputs.metadata.query_column(first).get('name', None)

            names = set()
            for other in others:
                other_metadata = outputs.metadata.query_column(other)
                # We do not care about empty strings for names either.
                if other_metadata.get('name', None):
                    if first_name != other_metadata['name']:
                        names.add(other_metadata['name'])

            first_other_names = list(outputs.metadata.query_column(first).get('other_names', []))
            first_other_names += sorted(names)
            if first_other_names:
                outputs.metadata = outputs.metadata.update_column(first, {
                    'other_names': first_other_names,
                }, source=self)

        # We flatten all values of "equal_columns_map" into one list.
        outputs = utils.remove_columns(outputs, list(itertools.chain.from_iterable(equal_columns_map.values())), source=self)

        return base.CallResult(outputs)

    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> typing.List[int]:
        def can_use_column(column_index: int) -> bool:
            return True

        columns_to_use, columns_not_to_use = utils.get_columns_to_use(inputs_metadata,  hyperparams['use_columns'], hyperparams['exclude_columns'], can_use_column)

        return columns_to_use

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        # In this case we cannot really do much without having access to data, so let's assume no column is duplicate.
        outputs_metadata = inputs_metadata

        return outputs_metadata
