import collections
import os
import typing

import pandas  # type: ignore

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams, problem
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ComputeScoresPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    metrics = hyperparams.Set(
        elements=hyperparams.Hyperparams.define(collections.OrderedDict(
            metric=hyperparams.Enumeration(
                values=[metric.name for metric in problem.PerformanceMetric],
                # Default is ignored.
                # TODO: Remove default. See: https://gitlab.com/datadrivendiscovery/d3m/issues/141
                default='ACCURACY',
            ),
            pos_label=hyperparams.Hyperparameter[typing.Union[str, None]]('1'),
            k=hyperparams.Hyperparameter[typing.Union[int, None]](20),
        )),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of metrics to compute.",
    )


# TODO: Add "can_accept".
class ComputeScoresPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive that takes a DataFrame with predictions and a scoring Dataset and computes
    scores for given metrics and outputs them as a DataFrame.

    It searches only the dataset entry point resource for target columns
    (which should be marked with ``https://metadata.datadrivendiscovery.org/types/TrueTarget``
    semantic type) in the scoring Dataset.

    The primitive uses the target column names as the identifier of the target columns.
    Specifically, the column names are acquired from the metadata, when possible, with
    fallback to column names from underlying DataFrame.

    The primitive makes sure that rows are properly aligned based on the ``d3mIndex``
    column (or more precisely, based on the column with
    ``https://metadata.datadrivendiscovery.org/types/PrimaryKey`` semantic type).
    """

    __author__ = 'Mingjie Sun <sunmj15@gmail.com>'
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '799802fb-2e11-4ab7-9c5e-dda09eb52a70',
            'version': '0.1.0',
            'name': "Compute scores given the metrics to use",
            'python_path': 'd3m.primitives.evaluation.ComputeScores',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:sunmj15@gmail.com',
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                 metadata_base.PrimitiveAlgorithmType.ACCURACY_SCORE,
                 metadata_base.PrimitiveAlgorithmType.F1_SCORE,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.EVALUATION,
        },
    )

    def produce(self, *, inputs: Inputs, score_dataset: container.Dataset, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        if not self.hyperparams['metrics']:
            raise ValueError("\"metrics\" hyper-parameter cannot be empty.")

        true_targets, additional_indices_length = self._get_true_targets(score_dataset)
        predicted_targets = self._get_predicted_targets(inputs, true_targets, additional_indices_length)

        assert list(true_targets.columns) == list(predicted_targets.columns) or list(true_targets.columns) + ['confidence'] == list(predicted_targets.columns),\
            (list(true_targets.columns), list(predicted_targets.columns))

        outputs: typing.Dict[str, typing.List] = {
            'metric': [],
            'targets': [],
            'value': [],
        }

        for metric_configuration in self.hyperparams['metrics']:
            metric = problem.PerformanceMetric[metric_configuration['metric']]
            params = {}
            for param_name, param_value in metric_configuration.items():
                if param_name == 'metric':
                    continue
                if param_value is None:
                    continue
                params[param_name] = param_value

            additional_indices_columns = list(true_targets.columns[1:1 + additional_indices_length])
            columns = list(true_targets.columns[1 + additional_indices_length:])
            # There is at most one, in fact, but we still want a list.
            confidence_columns = list(predicted_targets.columns[len(true_targets.columns):])

            if metric.applicability_to_targets():
                predicted_data = predicted_targets[columns].values
                # This works well if "true_targets" column is a string and has to be cast to int or float,
                # but it does not work if the column is an int but it should be cast to a string, because it
                # will just cast it to object, but leave it as an int.
                true_data = true_targets[columns].values.astype(predicted_data.dtype)

                # Pass only target columns in. So no "d3mIndex" column, additional index columns, or
                # "confidence" column. The later two should not probably even be there.
                # Because we are using columns from "true_targets", we will skip the "confidence".
                score = metric.get_function()(true_data, predicted_data, **params)

                outputs['metric'].append(metric.name)
                outputs['targets'].append(','.join(columns))
                outputs['value'].append(score)

            else:
                # Because we are iterating over columns from "true_targets", we will skip the "confidence"
                # column if there is any in "predicted_targets". We skip also over any additional primary
                # index columns.
                for column in columns:
                    predicted_data = predicted_targets[additional_indices_columns + [column] + confidence_columns].values
                    # This works well if "true_targets" column is a string and has to be cast to int or float,
                    # but it does not work if the column is an int but it should be cast to a string, because it
                    # will just cast it to object, but leave it as an int.
                    true_data = true_targets[additional_indices_columns + [column]].values.astype(predicted_data.dtype)

                    # We pass in also additional index columns and "confidence" column if it is there.
                    score = metric.get_function()(true_data, predicted_data, **params)

                    outputs['metric'].append(metric.name)
                    outputs['targets'].append(column)
                    outputs['value'].append(score)

        results = container.DataFrame(data=outputs, columns=['metric', 'targets', 'value'], generate_metadata=False, source=self)
        results.metadata = inputs.metadata.set_for_value(results, generate_metadata=True, source=self)

        # Set column names.
        results.metadata = results.metadata.update_column(0, {'name': 'metric'}, source=self)
        results.metadata = results.metadata.update_column(1, {'name': 'targets'}, source=self)
        results.metadata = results.metadata.update_column(2, {'name': 'value'}, source=self)

        # Multi-key table.
        results.metadata = results.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/PrimaryKey', source=self)
        results.metadata = results.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/PrimaryKey', source=self)
        results.metadata = results.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/Score', source=self)

        return base.CallResult(results)

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, score_dataset: container.Dataset,  # type: ignore
                      timeout: float = None, iterations: int = None) -> base.MultiCallResult:
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, score_dataset=score_dataset)

    # TODO: Instead of extracting true targets only from the dataset entry point, first denormalize and then extract true targets.
    #       How to know which primary index columns to use then?
    def _get_true_targets(self, score_dataset: container.Dataset) -> typing.Tuple[pandas.DataFrame, int]:
        """
        Extracts true targets from the Dataset's entry point, or the only tabular resource.
        The first column is ``d3mIndex``. Then any other primary index columns follow. Then true targets.

        We return a regular Pandas DataFrame with column names matching those in the metadata.
        And the number of additional primary index columns.
        """

        main_resource_id, main_resource = utils.get_tabular_resource(score_dataset, None, has_hyperparameter=False)

        dataframe = self._to_dataframe(main_resource)

        indices = list(utils.get_index_columns(score_dataset.metadata, at=(main_resource_id,)))
        targets = list(utils.list_columns_with_semantic_types(score_dataset.metadata, ['https://metadata.datadrivendiscovery.org/types/TrueTarget'], at=(main_resource_id,)))

        if not indices:
            raise ValueError("No primary index columns.")
        if not targets:
            raise ValueError("No (true) target columns.")

        dataframe = dataframe.iloc[:, indices + targets]

        if d3m_utils.has_duplicates(dataframe.columns):
            duplicate_names = list(dataframe.columns)
            for name in set(dataframe.columns):
                duplicate_names.remove(name)
            raise ValueError("Target columns have duplicate names: {duplicate_names}".format(
                duplicate_names=sorted(set(duplicate_names)),
            ))

        if 'confidence' in dataframe.columns:
            raise ValueError("Target column cannot be named \"confidence\". It is a reserved name.")

        return dataframe, len(indices) - 1

    def _to_dataframe(self, inputs: container.DataFrame) -> pandas.DataFrame:
        # We have to copy, otherwise setting "columns" modifies original DataFrame as well.
        dataframe = pandas.DataFrame(inputs, copy=True)

        column_names = []
        for column_index in range(len(inputs.columns)):
            column_names.append(inputs.metadata.query_column(column_index).get('name', inputs.columns[column_index]))

        # Make sure column names are correct.
        dataframe.columns = column_names

        return dataframe

    def _get_predicted_targets(self, inputs: Inputs, true_targets: pandas.DataFrame, additional_indices_length: int) -> pandas.DataFrame:
        """
        Make sure predicted targets match true targets in the order of rows and target columns.
        The first column is ``d3mIndex``, followed by any other primary index columns.
        Puts ``confidence`` column at the end, if it exists. Only one ``confidence`` column
        is supported.

        We return a regular Pandas DataFrame with column names matching those in the metadata.
        """

        dataframe = self._to_dataframe(inputs)

        columns = list(true_targets.columns)

        # TODO: Maybe use a semantic type "https://metadata.datadrivendiscovery.org/types/Confidence" instead of column name.
        if 'confidence' in dataframe.columns:
            if len([column for column in dataframe.columns if column == 'confidence']) >= 2:
                raise ValueError("Duplicate \"confidence\" column.")

            assert 'confidence' not in columns
            columns.append('confidence')

        dataframe = dataframe[columns]

        if list(dataframe.columns) != columns:
            raise ValueError("Predicted target columns do not match true target columns. Predicted: {predicted} vs. true: {true}.".format(
                predicted=list(dataframe.columns),
                true=columns,
            ))

        # Columns now match, we just have to make sure rows match as well.
        if additional_indices_length == 0:
            if (dataframe.shape[0] != true_targets.shape[0]):
                raise ValueError("Predictions have a different number of samples than truth. Predictions: {predictions} vs. truth: {truth}.".format(
                    predictions=dataframe.shape[0],
                    truth=dataframe.truth[0],
                ))

            predicted_index = dataframe.iloc[:, 0]
            # This works well if "true_targets" column is a string and has to be cast to int or float,
            # but it does not work if the column is an int but it should be cast to a string, because it
            # will just cast it to object, but leave it as an int.
            true_index = true_targets.iloc[:, 0].astype(predicted_index.dtype)

            return dataframe.set_index(predicted_index).reindex(true_index).reset_index(drop=True)

        # When additional primary index columns are around, we do not do anything,
        # because number of rows do not have to match, nor the order.
        # TODO: All this should be done better, see: https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/issues/117
        #       For real multi-index dataframes, we should make sure rows match based on those indices.
        #       We can do that with something like:
        #       dataframe_index_series = [dataframe.iloc[:, dataframe_index] for dataframe_index in range(additional_indices_length + 1)]
        #       true_targets_index_series = [true_targets.iloc[:, true_targets_index] for true_targets_index in range(additional_indices_length + 1)]
        #       dataframe = dataframe.set_index(dataframe_index_series).reindex(true_targets_index_series).reset_index(drop=True)

        return dataframe
